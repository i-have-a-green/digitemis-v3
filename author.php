<?php
/**
 * The template for displaying author info
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */
get_header(); ?>

<div class="white-bg">

    <div class="has-hero">
        <h1 class="wrapper-narrow white-bg"><?php _e("Le blog de ",'digitemis') ?><?php echo get_bloginfo( 'name' );?></h1>
        <?php get_template_part('template-parts/author-bio'); ?>
    </div>
    <section class="wrapper white-bg above-hero">
        <?php if ( have_posts() ) : ?>

<!--             <nav id="category-blog">
                <?php echo ihag_menu('category'); ?>
            </nav> -->
            
            <div class="author-posts">
                <div class="post-container articles block-spacing" id="infinite-list">
                <?php
                    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $articles = get_posts( array(
                        'paged'             => $paged,
                        'posts_per_page'    => get_option( 'posts_per_page' ),
                        'post_type'      => 'post',
                        'post_status'    => 'publish',
                        'author'         => $curauth->ID
                    ) );
                    global $post;
                    foreach($articles as $article){
                        $post = get_post($article->ID);
                        get_template_part( 'template-parts/content', get_post_type() );
                    }
                    wp_reset_postdata();
                ?>
                </div>

                <div>
                    <div id="contLoaderPost">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/loader.png" id="loaderPost" alt="loader">
                    </div>
                </div>
            </div>

        <?php endif;?>
    </section><!-- Posts Content -->
</div>
<?php get_footer(); ?>
window.addEventListener('scroll', function(e) {
    var els = document.getElementsByClassName("animation");
    Array.prototype.forEach.call(els, function(elm) {
        var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        load = false;
        if (!load && (elm.offsetTop + (elm.clientHeight / 3)) < (window.scrollY + height)) { //scroll > bas de elem
            load = true;
            elm.classList.add("active");
        }
    });
});
/*if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href; 
    var elm = document.querySelector('#infinite-list');
    loader = document.querySelector('#contLoaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    window.addEventListener('scroll', function(e) {
        if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
            //inserer les éléments suivants
            load = true;
            readMorePost();
        }
    });

}


function readMorePost() {
    page++;
    loader.classList.add("active");
    var formData = new FormData();
    formData.append("action", "readMorePost");
    formData.append("page", page);
    formData.append("category", elm.dataset.category);
    xhr = new XMLHttpRequest();
    xhr.open('POST', ajaxurl, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            if (xhr.response != "") {
                console.log(xhr.response);
                elm.insertAdjacentHTML('beforeend', xhr.response);
                load = false;
                window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            }
            loader.classList.remove("active");
        }
        console.log(xhr.response);
    };
    xhr.send(formData);
}*/
/*
@source : https://www.w3schools.com/howto/howto_js_slideshow.asp

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
*/
if (document.forms.namedItem("contactForm")) {


    var el = document.getElementById('email');

    el.addEventListener("keyup", function (event) {
        var email = el.value;
        if(
          email.match(/@gmail.com/i) ||
          email.match(/@gmail.fr/i) ||
          email.match(/@hotmail.com/i) ||
          email.match(/@hotmail.fr/i) ||
          email.match(/@live.com/i) ||
          email.match(/@msn.com/i) ||
          email.match(/@aol.com/i) ||
          email.match(/@yahoo.com/i) ||
          email.match(/@yahoo.fr/i) || 
          email.match(/@inbox.com/i) ||
          email.match(/@gmx.com/i) ||
          email.match(/@gmx.fr/i) ||
          email.match(/@mail.ru/i) ||
          email.match(/@rambler.ru/i) ||
          email.match(/@test4xrumer.store/i) ||
          email.match(/@yandex.ru/i) ||
          email.match(/@neuf.fr/i) ||
          email.match(/@sfr.fr/i) ||
          email.match(/@me.com/i) 
        ){
            el.setCustomValidity("Veuillez renseigner une adresse email professionnelle");
        } else {
            el.setCustomValidity("");
        }
    });

    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();


       

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                window.dataLayer.push({
                    "event":"contact-form-success"
                });
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("contactRecrutement")) {
    document.forms.namedItem("contactRecrutement").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactRecrutement");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactRecrutement', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("contactLivreBlanc")) {
    document.forms.namedItem("contactLivreBlanc").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactLivreBlanc");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactLivreBlanc', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                window.dataLayer.push({
                    "event":"telechargement-livre-blanc"
                });
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("formRegistration")) {
    document.forms.namedItem("formRegistration").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("formRegistration");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'formRegistration', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("footer-newslettter-form")) {
    document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
        document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
        e.preventDefault();
        var form = document.forms.namedItem("footer-newslettter-form"); 
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'formNewsletter', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            console.log(xhr.status);
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('sendMessageNewsletter').disabled = false;
                document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageNewsletter').innerHTML = xhr.response.replace('"', '').replace('"', '');
                document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
                window.dataLayer.push({
                    "event":"newsletter-success"
                });
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("wpcf7-f49253-p49254-o1")) {
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        window.dataLayer.push({
            "event":"telechargement-fiche-pratique"
        });
    }, false );
}

if (document.getElementById("wpcf7-f49425-p49357-o1")) {
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        window.dataLayer.push({
            "event":"telechargement-presentation"
        });
    }, false );
}


/* document.addEventListener('DOMContentLoaded', function() {
    addButtonDenyAllTarteaucitron(0);
});

function addButtonDenyAllTarteaucitron(tentative) {
    if (tentative < 20) {
        if (typeof tarteaucitron == 'undefined') {
            setTimeout(function() {
                addButtonDenyAllTarteaucitron(tentative + 1);
            }, 250);
        } else if (!document.getElementById("tarteaucitronAlertBig")) {
            setTimeout(function() {
                addButtonDenyAllTarteaucitron(tentative + 1);
            }, 250);
        } else {
            console.log("tarteaucitronAlertBig ok");
            var node = document.createElement("button");
            node.id = 'tarteaucitronDenyAll';
            node.onclick = function() {
                tarteaucitron.userInterface.respondAll(false);
            };
            var t = document.createTextNode("Non merci");
            node.appendChild(t);
            document.getElementById("tarteaucitronAlertBig").appendChild(node);

            var p = document.getElementById("tarteaucitronPersonalize").innerHTML;
            document.getElementById("tarteaucitronPersonalize").innerHTML = p.replace('✓', '');
        }
    }

} */
/* var inputEmail = document.querySelectorAll('.wpcf7-form input[type=email]');

inputEmail.forEach(function(el){
    el.setAttribute('pattern','^(?!.*(gmail|yahoo|free)).*');
});
 */
document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("link-menu").addEventListener("click", function(e) {
        e.preventDefault();
        document.getElementById("menu-mobile-container").classList.toggle("openMenu");
        document.getElementById("link-menu").classList.toggle("close-menu");
    });
    //document.getElementById("menu").addEventListener("click", function(e) {
    //    document.getElementById("menu").classList.remove("openMenu");
    //});
});

// topbar gets sticky on scroll
var scroll = document.getElementById("masthead");
var menu = document.getElementById("masthead");
var sticky = 1;
window.onscroll = function() {
    if (window.pageYOffset >= sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
};
document.addEventListener("DOMContentLoaded", function(event) {
    if (window.pageYOffset >= sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
});

function toggleSearchbar() {
    document.getElementById("form-search").classList.toggle("openSearchbar");
}


var els = document.getElementsByClassName("js-change-categorie");
Array.prototype.forEach.call(els, function(elm) {
    elm.addEventListener("click", function(e) {
        noActive(els);
        elm.classList.add("active");
        termIdTestimonial = elm.dataset.category;
        var testimonials = document.getElementsByClassName("testimonial-card");
        Array.prototype.forEach.call(testimonials, function(testimonial) {
            console.log(testimonial.dataset.termid + " - " + termIdTestimonial);

            if (testimonial.dataset.termid == termIdTestimonial) {
                testimonial.classList.add("active");
                testimonial.classList.remove("hidden");
            } else {
                testimonial.classList.add("hidden");
                testimonial.classList.remove("active");
            }
        });

    });
});


function noActive(elms) {
    Array.prototype.forEach.call(elms, function(elm) {
        elm.classList.remove("active");
    });
}
document.addEventListener('DOMContentLoaded', function () {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) { 
            e.stopPropagation();
            e.preventDefault();
            var width  = 320, 
            height = 400,
            left   = (widthScreen - width)  / 2, 
            top    = (heightScreen - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                             ',width='  + width  +
                             ',height=' + height +
                             ',top='    + top    +
                             ',left='   + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });
});

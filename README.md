# Thème Digitemis 

Prérequis : 

- Node.js (https://nodejs.org/) 
- npm (https://www.npmjs.com/) 
- Wordpress (http://wordpress.org/latest.tar.gz) 
- Thème Parent Susty (https://github.com/jacklenox/susty/archive/master.zip) 

1 - Installer Wordpress 

2 - Installer les pluggins suivants : 
``` 
ACF PRO : https://www.advancedcustomfields.com/
Custom Post Type UI : https://fr.wordpress.org/plugins/custom-post-type-ui/
Category Order and Taxonomy Terms Order : https://fr.wordpress.org/plugins/taxonomy-terms-order/
``` 

3 - Installer et activer le thèmse Susty (via l'admin Wordpress) 

4 - Installer et activer le thème Digitemis à l'emplacement suivant : 
``` 
$ / wp-content / themes / digitemis 
``` 

5 - Installer les dépendances avec npm 
```
$ npm install 
``` 

6 - Lancer le script qui compile les fichiers
```
$ npm run watch 
```

7 - Lancer BrowserSync [optionnel] 
```
$ npm run start 
```

# Notes complémentaires : 

Retirer les thèmes Wordpress inutiles (twentytwenty, twentynineteen, ect …) 
``` 
$ cd wp-content/themes/ $ rm -rf twenty 
``` 

Modifier le chemin des scripts en fonction de l'environnement local<br/>
Chemin par défaut : 
```
'https://digitemis.local/' 
``` 
dans package.json (ligne 44) 

Ne pas oublier de synchroniser les champs ACF et les les posts CPTUI pendant le développement<br/>
ACF PRO - b3JkZXJfaWQ9OTE1Mzh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTEwLTEyIDE0OjMyOjI3


<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>


<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
    <article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Breadcrumb -->
        <div class="wrapper">
            <?php get_template_part( 'template-parts/content', 'hero' ); ?>
        </div>

            <!-- Testimonial-title -->
        <section class="wrapper above-hero">
            <div class="sub-wrapper white-bg">
                <h1 class="page-title no-padding center"><?php the_title();?></h1>                
            </div>
        </section>
            
        <!-- Testimonial-content -->
        <div id="sub-wrapper-content" class="above-hero">
            <?php the_content();?>
        </div>
        
    </article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();
?>
<?php
global $post;
$page404 = get_field('page_404', 'option');
if( $page404 ):
    $post = get_post($page404); setup_postdata($post);?>
    <?php get_header();?>

    <main id="main">
        <?php get_template_part( 'template-parts/content', get_post_type() );?>
    </main><!-- #main -->
    
    <?php get_footer(); ?>
    
<? else:
    wp_redirect(home_url(), 301);
endif;

?>
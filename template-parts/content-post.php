<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<?php 
setup_postdata( $post ); 
$term_obj_list = get_the_terms($post, 'category');
$cat = join(', ', @wp_list_pluck($term_obj_list, 'name'));
?>

<article class="card-post">
	<a class="post-link" href="<?php the_permalink();?>" title="<?php the_title();?>"></a>
	<?php 
	if ( has_post_thumbnail() ) {
		the_post_thumbnail('thumb-post'); 
	} else {
		$image = get_field('thumb-fallback', 'option');
		$size = 'thumb-post';
		if( $image ) {
		echo wp_get_attachment_image( $image, $size );
		}
	} 
	?>
		<!-- <div class="tag-links">
			<?php //echo $cat;
			
			$cats = get_the_terms($post, "category");
			if($cats){
				_e("Catégories :",'digitemis');
				foreach ( $cats as $cat ) :?>
					<a href="<?php echo get_term_link($cat);?>" class="tag-link">
						<?php echo '#'.$cat->name . '';?>
				</a> 
				<?php endforeach;
			}
			?>
	</div>
	<div class="tag-links">
		<?php //echo $cat;
		
		$terms = get_the_terms($post, "post_tag");
		if($terms){
			_e("Étiquettes :",'digitemis');
			foreach ( $terms as $term ) :?>
				<a href="<?php echo get_term_link($term);?>" class="tag-link">
					<?php echo ''.$term->name . '';?>
			</a> 
			<?php endforeach;
		}
		?>
	</div> -->
	
	<h3><?php the_title(); ?></h3>
	<time datetime="<?php echo get_the_date('c');?>2012-02-11"><?php echo get_the_date();?></time>
	<div class="post-excerpt"><?php the_excerpt();?></div>
	<a class="read-more"  href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php _e("Lire l'article", "digitemis");?>
	</a>
</article>


<?php wp_reset_postdata(); ?>
<!-- Newsletter form here -->
<div id="footer-top-card">
    <h2 class="h3-like center"><?php echo sprintf(__("Suivez l'expertise %s en vous abonnant à la newsletter", "digitemis"), get_bloginfo( 'name' )); ?></h2>
    <form id="footer-newslettter-form">
        <input type="hidden" name="honeyPotWhitePaper" value="">
        <input type="email" class="input-brd" name="newletter_email" id="newletter_email" placeholder="<?php _e("Entrez votre e-mail", "digitemis"); ?>" required>
        <input class="button no-margin" type="submit" id="sendMessageNewsletter" value="<?php _e("S'inscrire", "digitemis")?>">
        <p id="ResponseMessageNewsletter"></p>
        <i class="formInfo">
            <?php echo sprintf(__("Les informations recueillies à partir de ce formulaire sont traitées par %s pour vous inscrire à sa newsletter.<br/> Pour connaître vos droits et la politique de %s sur la protection des données,", "digitemis"), get_bloginfo( 'name' ),get_bloginfo( 'name' ));?>
            <a href="<?php echo get_privacy_policy_url() ?>">
                <?php _e("Cliquez ici", "digitemis");?>
            </a>
        </i>
    </form>
</div>
<!-- Newsletter form here -->

<div id="footer-top-card">
    <h2 class="h3-like center"><?php echo sprintf(__("%s recrute", "digitemis"),get_bloginfo( 'name' )); ?> !</h2>
    <?php 
    $recrutement = get_field('link_recrutement', 'option');
    if( $recrutement ): 
        $recrutement_url = $recrutement['url'];
        $recrutement_title = $recrutement['title'];
        $recrutement_target = $recrutement['target'] ? $recrutement['target'] : '_self';
        ?>
        <a class="button uppercase" href="<?php echo esc_url( $recrutement_url ); ?>" target="<?php echo esc_attr( $recrutement_target ); ?>"><?php echo esc_html( $recrutement_title ); ?></a>
    <?php endif; ?>
</div>
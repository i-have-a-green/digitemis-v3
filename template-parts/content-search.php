<?php
/**
 * Template part for displaying search results search.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */
?>

<?php 
	$postType = get_post_type_object(get_post_type());
	setup_postdata( $post ); 
	$terms = get_the_terms($post, 'ville');
	$ville = $terms[0];
?>

<div class="search-card" href="<?php the_permalink();?>" title="<?php the_title();?>">
	<h3>
		<?php echo esc_html($postType->labels->singular_name); ?>
		 &laquo; <?php the_title(); ?> &raquo;
	</h3>
	<a href="<?php the_permalink();?>" title="<?php the_title();?>" class="button no-margin button-brd-white uppercase">
		<?php _e("Voir", "digitemis");?>
	</a>
</div>
<?php wp_reset_postdata();?>

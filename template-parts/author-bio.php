<?php
/**
 * Template part for displaying author infos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */
?>

<div class="wrapper-narrow white-bg author-template">
    <div class="author-bio <?php echo get_option( 'show_avatars' ) ? 'show-avatars' : ''; ?>">
        <div class="author-header">
            <?php echo get_avatar( get_the_author_meta( 'ID' ), '85' ); ?>
            <h2 class="author-title"><?php printf( get_the_author() ); ?></h2>
        </div>
        <div class="author-bio-content">
            <p class="author-description"> <?php the_author_meta( 'description' ); ?></p><!-- .author-description -->
<!--             <?php
            printf(
                '<a class="author-link" href="%1$s" rel="author">' . esc_html__( 'Voir les autres article de %2$s.', 'digitemis' ) . '</a>',
                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                get_the_author()
            );
            ?> -->
        </div><!-- .author-bio-content -->
    </div><!-- .author-bio -->
</div>
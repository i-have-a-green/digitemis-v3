<?php
/**
* Block Name: Bloc training
*/
?>
<section class="training-bloc step-use-case full-width">
<?php
$content = get_field('content');
if ( !$content ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-welcome-learn-more"></span><br>
		<b>Formation : Contenu</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper">
		<div class="training-loop">
			<div class="training-loop-item">
				<div class="grid-content">
					<h2 class="underline small-margin left-icon" ><?php _e("Contenu");?></h2>
					<div class="entry-content">
						<?php the_field("content");?>
					</div>
					<h2 class="underline small-margin left-icon last-title"><?php _e("Objectifs");?></h2>
					<?php the_field("target");?>
				</div>
				<div class="grid-illustration">
					<div class="blue-background-trick entry-content">
						<?php the_field("blue-content");?>
					</div>
					<div class="image-illustration training-bg-trick">
						<?php
							$image = get_field("image");
							$size = 'free-height';
							if( $image ) {
								echo wp_get_attachment_image( $image, $size );
							}
						?>
					</div>
				</div>
			</div>
		</div>
	
		

		
	</div>
<?php endif;
?>
</section>

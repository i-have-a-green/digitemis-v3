<?php
/**
* Block Name: Bloc steps-line
*/
?>
<section class="steps-line">
<?php
$slides = get_field('image');
if ( !$slides ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-format-image"></span><br>
		<b>Visuel 4 étapes (Responsive)</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper">
	<?php
			$image = get_field("image");
			$size = 'free-height';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size, false, array('class'=>'desktop-picture tiny-hidden') );
			}
		?>
		<?php
			$image = get_field("image-mobile");
			$size = 'free-height';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size, false, array('class'=>'mobile-picture desktop-hidden tablet-hidden mobile-xl-hidden ') );
			}
		?>
	</div>
	<?php
endif;
?>
</section>

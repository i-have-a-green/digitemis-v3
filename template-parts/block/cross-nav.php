<?php
/**
* Block Name: Bloc cross-nav
*/
?>
<section class="cross-nav full-width">
<?php
$navs = get_field('navs');
if ( !$navs ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-admin-links"></span><br>
		<b>Bloc Navigation croisée</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper center bloc-vertical-spacing">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
		<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title h2-like"><?php the_field('title');?><br>
			<span class="section-subtitle"><?php the_field('subtitle');?></span>
		</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="nav-loop">
		<?php foreach ($navs as $key => $nav) :
			?>
			<div class="nav-loop-item nav-loop-item-hover">
				<div class="way-loop-item-illustration">
					
						<?php
							$image = $nav['image'];
							$size = '80-80';
							if( $image ) {
								echo wp_get_attachment_image( $image);
							}
						?>
					<a href="<?php echo $nav['link'];?>" class="center">
						<?php echo $nav['label'];?>
					</a>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>
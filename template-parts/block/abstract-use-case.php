<?php
/**
* Block Name: Bloc abstract-use-case
*/
?>
<section class="abstract-use-case full-width">
<?php
$items = get_field('items');
if ( !$items ) :
	?>
	<div style="text-align:center; color: white;">
	<span class="dashicons dashicons-editor-ul"></span><br>
		<b>Bloc Synthèse (UseCase)</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper white bloc-vertical-spacing">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title center h2-like"><?php the_field('title');?><br>
				<span class="section-subtitle center"><?php the_field('subtitle');?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="items-abstract-case-loop">
		<?php foreach ($items as $key => $item) :
			?>
			<div class="items-abstract-case-loop-item center">
				<div class="items-abstract-case-loop-item-illustration">
					<?php
						$image = $item['image'];
						$size = '80-80';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size, "", array( "class" => "white-img" ));
						}
					?>
				</div>
				<div class="items-use-case-loop-item-content small-content small-content-no-title big-f-size">
					<?php if ($item['title']) { ?>
						<h3 class="small-content-title">
							<?php echo $item['title'];?><br>
						</h3>
					<?php
					} ?>

					<?php echo $item['content'];?>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>

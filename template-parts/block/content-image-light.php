<?php
/**
* Block Name: Bloc content-image
*/
?>
<section class="content-image-light">

<?php
$presentation = get_field('image');
if ( !$presentation ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-format-image"></span><br>
		<b>Bloc Contenu / Image - Light</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper">
		<?php 
		$title = get_field( 'title' );
		$subtitle = get_field( 'subtitle' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="page-title h2-like">
				<?php echo $title;?><br>
				<span class="light-blue"><?php echo $subtitle;?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php 
		} ?>
		<div class="content-image-light-layout">
			<div class="content-image">
				<?php
				$image = get_field("image");
				$size = 'free-height';
				if( $image ) {
					echo wp_get_attachment_image( $image, $size );
				}
				?>
			</div>
			<div class="content-text">
				<?php the_field ('content'); ?>
			</div>
		</div>
	</div>
	<?php
endif;
?>
</section>

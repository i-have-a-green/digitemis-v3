<?php
/**
* Block Name: Bloc testimonial
*/
?>
<section class="banner-testimonial bg-testimonial full-width 
	<?php
	$animation = get_field('animation');
	if ( $animation ) {
		echo 'animation';
	}
	?>
	"
	<?php
	$img = get_field('image');
	$bg_color = get_field('bg_color');
	
	if ( $img ) { ?> 
		style="background-image:url(<?php echo $img;?>)"
	<?php 
	} if ( $bg_color ) { ?>
		style="background:<?php echo $bg_color;?>"
	<?php } ?>
	>
<?php
$content = get_field("content");
$author = get_field("author");
$link = get_field('link-button');
if ( !$content AND !$author AND !$link ) :
	?>
	<div style="text-align:center" class="white">
	<span class="dashicons dashicons-admin-links"></span><br>
		<b>Bloc Bannière Témoignage</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="bg-testimonial-above white <?php if ( $img ) { echo 'has-bg-image'; }?> ">
		<div class="wrapper center">
			<?php

			if ($content) { ?>
				<div class="baseline">
					<?php echo $content;?>
				</div>
			<?php
			}
			if ($author) { ?>
				<div class="testimonial-author <?php if ( get_field("white-author")  == true ) { echo 'white-author';} ?>">
					<?php the_field("author");?>
				</div>
			<?php
			}
			?>

			<?php if(get_field('label-button') && get_field('link-button')):?>
				<a href="<?php echo get_field('link-button');?>" class="button button-brd-white uppercase">
					<?php echo get_field('label-button');?>
				</a>
			<?php endif;?>
		</div>
	</div>
	<?php
	endif;
?>
</section>

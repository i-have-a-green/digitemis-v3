<?php
/**
* Block Name: Bloc carrousel Home
*/
?>
<section class="carrousel-home white">
<?php
$slides = get_field('slides');
if ( !$slides ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-slides"></span><br>
		<b>Carroussel Home</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	foreach ($slides as $key => $slide) :
		?>
		<?php
			$image = $slide['bg-image'];
			$size = 'carrousel-bg';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}
		?>
		<div class="slide fade" id="slide-<?php echo $key;?>" style="background-image:url(<?php echo $slide['bg-image'];?>)">
			<div class="wrapper slide-layout">
				<div class="slide-left animation active">
					<<?php echo($slide['head_level'])?$slide['head_level']:'span';?>><?php echo $slide['title'];?></<?php echo($slide['head_level'])?$slide['head_level']:'span';?>>
					<div class="entry-content">
						<?php echo $slide['content'];?>
					</div>
					<?php if($slide['label-button'] && $slide['link-button']):?>
						<a href="<?php echo $slide['link-button'];?>" class="button">
							<?php echo $slide['label-button'];?>
						</a>
					<?php endif;?>
				</div>
				<div class="slide-right">
					<?php
						$image = $slide['image'];
						$size = 'free-height';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}
					?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>

		<!-- The dots/circles -->
		<div id="dot-container" class="right">

		<?php 
			$slide_count = count(get_field('slides'));
			$count = 1;
			foreach ($slides as $key => $slide) :

				if ($count <= $slide_count) {
					echo '<span class="dot" onclick="currentSlide('. $count .')"></span>';
					$count++;
				}
			endforeach;		
		?>

		</div>

		<script>
			var slideIndex = 1;
			showSlides(slideIndex);

			// Next/previous controls
			function plusSlides(n) {
			showSlides(slideIndex += n);
			}

			// Thumbnail image controls
			function currentSlide(n) {
			showSlides(slideIndex = n);
			}

			function showSlides(n) {
			var i;
			var slides = document.getElementsByClassName("slide");
			var dots = document.getElementsByClassName("dot");
			if (n > slides.length) {slideIndex = 1}
			if (n < 1) {slideIndex = slides.length}
			for (i = 0; i < slides.length; i++) {
				slides[i].style.display = "none";
			}
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex-1].style.display = "block";
			dots[slideIndex-1].className += " active";
			} 

			// Automatic animation (on desktop only)
			if (window.innerWidth > 800) {		
				setTimeout(function(){ plusSlides(1); }, 4000);
				setTimeout(function(){ plusSlides(1); }, 8000);
				setTimeout(function(){ plusSlides(1); }, 16000);
			}

		</script>
		
<?php endif; ?>
</section>

<?php
/**
* Block Name: Bloc content-image
*/
?>
<section class="content-image
	<?php
	$animation = get_field('animation');
	if ( $animation ) {
		echo 'animation ';
		the_field("presentation");
	}
	?>
	">

<?php
$presentation = get_field('image');
if ( !$presentation ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-format-image"></span><br>
		<b>Bloc Contenu / Image</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper white-bg content-image-wrapper bloc-vertical-spacing <?php the_field("presentation");?> <?php the_field("proportion");?>  <?php echo(get_field("element_graphique"))?'contain-element-graphique':'';?>">
		<div class="content-image-content">
			<?php 
			$title = get_field( 'title' );
			if( $title ){ ?>
				<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="underline small-margin">
					<?php the_field('title');?>
				</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
			<?php } ?>

			<div class="entry-content">
				<?php the_field('content');?>
			</div>
			<?php if(get_field('label-button') && get_field('link-button')):?>
				<a href="<?php echo get_field('link-button');?>" class="button button-brd-blue">
					<?php echo get_field('label-button');?>
				</a>
			<?php endif;?>
		</div>
		<div class="content-image-image <?php echo(get_field("element_graphique"))?'element-graphique':'';?> parent-<?php the_field("presentation");?> parent-<?php the_field("proportion");?>">
			<?php
			$image = get_field("image");
			$size = (get_field("element_graphique"))?'thumb-post':'free-height';
			if( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}
			?>
		</div>
	</div>
	<?php
endif;
?>
</section>

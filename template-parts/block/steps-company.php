<?php
/**
* Block Name: Bloc steps company
*/
?>
<section class="step-use-case steps-company bloc-vertical-spacing full-width">
<?php
$steps = get_field('steps');
if ( !$steps ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-clock"></span><br>
		<b>Bloc Historique de l'entreprise</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="h1-like underline"><?php the_field('title');?><br>
				<span class="section-subtitle"><?php the_field('subtitle');?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="steps-use-case-loop">
		<?php foreach ($steps as $key => $step) :?>
			<div class="steps-use-case-loop-item steps-use-case-loop-item-historic">
				<div class="steps-use-case-historic grid-content small-content">
					<h3 class="small-content-title">
						<?php echo $step['title'];?><br>
					</h3>
					<?php echo $step['content'];?>
				</div>
				<div class="steps-use-case-loop-item-illustration grid-illustration">
					<time class="steps-use-case-year"><?php echo $step['year'];?></time>
					<?php
						$image = $step['image'];
						$size = 'free-height';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size);
						}
					?>
					<?php
						$image = $step['image2'];
						$size = 'free-height';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size);
						}
					?>
				</div>
			</div><!-- .steps-item -->
			<?php endforeach;?>
		</div><!-- .steps-loop -->
	</div><!-- .wrapper -->
<?php endif;?>
</section>

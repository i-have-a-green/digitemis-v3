<?php
/**
* Block Name: Bloc pre footer
*/
?>
<section class="pre-footer white light-blue-bg bloc-vertical-spacing full-width">
<?php
$test = get_field('title');
if ( !$test ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-admin-links"></span><br>
		<b>Bloc Pré-footer (Témoignage)</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper center">
		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="baseline"><?php the_field("title");?></<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="prefooter-content">
			<?php the_field("content");?>
		</div>
		<?php if(get_field('label-button') && get_field('link-button')):?>
			<a href="<?php echo get_field('link-button');?>" class="button uppercase button-brd-white">
				<?php echo get_field('label-button');?>
			</a>
		<?php endif;?>
	</div>
	<?php
endif;
?>
</section>

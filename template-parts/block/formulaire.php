<?php
/**
* Block Name: Bloc Formulaire
*/
?>
<section class="formulaire wrapper-narrow-container full-width">
	<div class="wrapper-narrow white-bg narrow-form">
		<form class="form-white-background has-section" name="blocFormulaire" id="contactForm" action="#" method="POST">
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="center full-width h2-like"><?php the_field('title');?></<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
			<p class="center full-width" ><?php the_field('subtitle');?></p>
			<input type="hidden" name="honeyPotWhitePaper" value="">
			<div class=" half-width">
				<label for="name"><?php _e("Nom", "digitemis");?> *</label>
				<input type="text" name="name" id="name" placeholder="Garnier" required value="">
			</div>
			<div class=" half-width">
				<label for="firstname"><?php _e("Prénom", "digitemis");?> *</label>
				<input type="text" name="firstname" id="firstname" placeholder="Alain" required value="">
			</div>
			
			<div class=" half-width">
				<label for="email"><?php _e("Mail", "digitemis");?> *</label>
				<input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="">
			</div>
			
			<div class=" half-width">
				<label for="phone"><?php _e("Téléphone", "digitemis");?> *</label>
				<input type="tel" name="phone" id="phone" placeholder="06 XX XX XX XX" required >
			</div>

			<div class=" full-width">
				<label for="company"><?php _e("Société", "digitemis");?> *</label>
				<input type="text" name="company" id="company" placeholder="SARL Acme" required value="">
			</div>

			<div class=" full-width">
				<label for="activity"><?php _e("Secteur d'activité", "digitemis");?> *</label>
				<input type="text" name="activity" id="activity" placeholder="<?php _e("Communication", "digitemis");?>" value="">
			</div>

			<div class=" full-width">
				<label for="function"><?php _e("Fonction", "digitemis");?> *</label>
				<input type="text" name="function" id="function" placeholder="<?php _e("Responsable technique", "digitemis");?>" required value="">
			</div>

			<!--
			<div class=" full-width">
				<label for="subject"><?php // _e("Sujet", "digitemis");?> *</label>
				<input type="text" name="subject" id="subject" placeholder="<?php // _e("Demande d'informations", "digitemis");?>" required value="">
			</div>
			-->

			<div class=" full-width">
				<label for="comment"><?php _e("Message", "digitemis");?> *</label>
				<textarea rows="5" name="comment" id="comment" placeholder="<?php _e("Rédigez votre message …", "digitemis");?>" required value=""></textarea>
			</div>

			<div id="ResponseAnchor" class="center full-width contain-button">
				<input class="button button-purple" type="submit" id="sendMessage" value="<?php _e("Envoyer", "digitemis");?>">
			<div id="ResponseMessage">
				<?php _e("Votre message a été envoyé !", 'IHAG');?>
			</div>

			<div class="full-width center">
				<i class="formInfo">* <?php _e("Informations obligatoires", "digitemis");?></i>
				<i class="formInfo"><?php echo sprintf(__("Les informations recueillies à partir de ce formulaire sont traitées par %s pour donner suite à votre demande de contact. Pour connaître vos droits et la politique de %s sur la protection des données,", "digitemis"), get_bloginfo( 'name' ),get_bloginfo( 'name' ));?>
					<a href="<?php echo get_privacy_policy_url() ?>">
					<?php _e("Cliquez ici", "digitemis");?>
					</a>
				</i>
			</div>
		</form>
	</div>
</section>

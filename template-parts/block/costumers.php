<?php
/**
* Block Name: Bloc costumers
*/
?>
<section class="costumers  
	<?php
	$animation = get_field('animation');
	if ( $animation ) {
		echo 'animation';
	}
	?>
	">
<?php
$costumers = get_field('costumers');
if ( !$costumers ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-images-alt"></span><br>
		<b>Bloc Logos Clients</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else : ?>
	<div class="wrapper bloc-vertical-spacing">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title underline center">
				<?php the_field('title');?>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="costumers-logos">
			<?
			foreach ($costumers as $key => $costumer) :

				$link = $costumer['link'];
				$image = $costumer['image'];
				$size = 'costumer';
				
				if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title ="<?php echo esc_html( $link_title ); ?>">
						<?php 
						if( $image ) :
						echo wp_get_attachment_image( $image, $size );
						else : 
						echo esc_html( $link_title );
						endif;
						?>
					</a>
				<?php 
				else : 
				echo wp_get_attachment_image( $image, $size );
				endif; 
			
			endforeach; ?>
		</div>
	</div>
		<?php endif; ?>
</section>

<?php
/**
* Block Name: Bloc way
*/
?>
<section class="way full-width">
<?php
$ways = get_field('ways');
if ( !$ways ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-editor-ul"></span><br>
		<b>Étapes</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper center white">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title h2-like"><?php the_field('title');?><br>
				<span class="section-subtitle"><?php the_field('subtitle');?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php 
		} ?>

		<div class="way-loop">
		<?php foreach ($ways as $key => $way) :
			?>
			<div class="way-loop-item">
				<div class="way-loop-item-illustration grid-illustration <?php if (get_field("number")) : echo 'has-number'; endif;?>">
					<span class="number"><?php echo (get_field("number"))?($key+1):'';?></span>
					<?php
						$image = $way['image'];
						$size = '80-80';
						if( $image ) {
							echo '<div class="way-loop-item-illustration-size hexagon-bg hexagon-blue">';
							echo wp_get_attachment_image( $image, $size, "", array( "class" => "white-img" ));
							echo '</div>';
						}
					?>
				</div>
				<div class="way-loop-item-content grid-content">
					<h3 class="underline underline-white small-margin <?php if ($way['subtitle']) : echo 'has-subtitle'; endif;?>">
						<?php echo $way['title'];?><br>
						<span><?php echo $way['subtitle'];?></span>
					</h3>
					<?php echo $way['content'];?>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
		<?php if(get_field('label-button') && get_field('link-button')):?>
			<?php 
				$link = get_field('link-button');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a href="<?php echo esc_url( $link_url ); ?>" <?php echo esc_url( $link_url ); ?> title="<?php echo esc_html( $link_title ); ?>" class="button button-brd-blue">
				<?php echo get_field('label-button');?>
			</a>
		<?php endif;?>
	</div>
<?php endif;
?>
</section>

<?php
/**
* Block Name: Bloc last-posts
*/
?>
<section class="last-posts  
	<?php
	$animation = get_field('animation');
	if ( $animation ) {
		echo 'animation';
	}
	?>
	">
	<div class="wrapper bloc-vertical-spacing">
		<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title center underline h2-like"><?php echo (empty(get_field('title'))) ? __("Derniers articles", 'digitemis') : get_field('title');?></<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php
		global $post;
			$lastposts = get_posts( array(
				'posts_per_page' => 2,
				'post_status'    => 'publish',
				'post__in'       => get_field("post"),
			) );
		?>
		<div class="post-container">
			<?php
				if ( $lastposts ) {
					foreach ( $lastposts as $post ) :
						
					get_template_part( 'template-parts/content', 'post' );
					
					endforeach; 
					wp_reset_postdata();
				}
			?>
		</div><!-- .post-container -->
	</div><!-- .wrapper -->
</section>

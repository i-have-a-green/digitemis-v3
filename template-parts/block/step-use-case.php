<?php
/**
* Block Name: Bloc step-use-case
*/
?>
<section class="step-use-case bloc-vertical-spacing full-width">
<?php
$steps = get_field('steps');
if ( !$steps ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-editor-ul"></span><br>
		<b>Bloc Étapes (UseCase)</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="h1-like underline"><?php the_field('title');?><br>
				<span class="section-subtitle"><?php the_field('subtitle');?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="steps-use-case-loop">
		<?php foreach ($steps as $key => $step) :
			?>
			<div class="steps-use-case-loop-item">
				<div class="steps-use-case-loop-item-content grid-content grid-content-is-centered">
					<div class="steps-use-case-number hexagon-bg-variante">
						<span class="number"><?php echo ($key+1);?></span>
						<?php
							$image = $step['picto'];
							$size = '80-80';
							if( $image ) {
								echo wp_get_attachment_image( $image, $size);
							}
						?>
					</div>
					<div class="steps-use-case-text small-content">
						<h3 class="small-content-title">
							<?php echo $step['title'];?><br>
						</h3>
						<?php echo $step['content'];?>
					</div>
				</div>
				<div class="steps-use-case-loop-item-illustration grid-illustration <?php echo($step["element"])?'has-element':'';?>">
					<?php
						$image = $step['illustration'];
						$size = 'free-height';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size);
						}
					?>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
		<?php if(get_field('label-button') && get_field('link-button')):?>
			<div class="center">
				<a href="<?php echo get_field('link-button');?>" class="button">
					<?php echo get_field('label-button');?>
				</a>
			</div>
		<?php endif;?>
	</div>
<?php endif;
?>
</section>

<?php
/**
* Block Name: Bloc recrutement
*/


?>
<section class="recrutement-bloc wrapper">

		
		<?php
			global $post;
			$args = array(
				'post_type'			=> 'recrutement',
				'posts_per_page' 	=> -1,
				'post_status'    	=> 'publish',
				'order' => 'ASC',
				'orderby' => 'menu_order' 
			);
			$myposts = get_posts( $args );
		?>
		<div class="post-container-regular bloc-vertical-spacing">

			<!-- Loop n°1 -->
			
			

			<?php
			$colors = array('#1cb7eb','#11425c','#3D8695','#1cb7eb','#11425c','#3D8695');
			$color_city = array();
			foreach ( $myposts as $post ) :
				setup_postdata( $post );
				$city = get_post_meta(get_the_id(), "city", true );
				if (!isset($color_city[$city])) {
					$color_city[$city] = $colors[0];
					array_shift($colors);
				}
				?>
				<div class="recrutement-card <?php if ( $application ) { echo "recrutement-card-application";} ?>">
					<div class="recrutement-city" style="background-color:<?php echo($color_city[$city]);?>;">
						<p class="bold xl"> 
							<?php echo get_post_meta(get_the_id(), "city", true );?>
						</p>
					</div>
					<div class="recrutement-job" style="color:<?php echo($color_city[$city]);?>;">
						<h3>
							<?php the_title();?>
						</h3>
						<a href="<?php the_permalink();?>" title="<?php the_title();?>" class="button button-brd-custom uppercase">
							<?php _e("Voir", "digitemis");?>
						</a>
					</div>
				</div>
			<?php
			endforeach; 
			wp_reset_postdata();
			?>
			<!-- Loop n°1 -->

			

		</div><!-- .post-container -->
</section>

<?php
/**
* Block Name: Bloc training label
*/
?>
<section class="training-label label">
<?php
$content = get_field('content');
if ( !$content ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-shield"></span><br>
		<b>Formation : Focus Label</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
<div class="wrapper bloc-vertical-spacing">
	<div class="label-container blue-bg">
		<div class="label-icon">
			<?php
					$image = get_field("picto");
					$size = '80-80';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size, "", array( "class" => "white-img values-img" ) );
					}
				?>
			</div>
		<div class="label-grid white">
			<div class="grid-illustration label-illustration">
				<?php
					$image = get_field("image");
					$size = 'free-height';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}
				?>
			</div>
			<div class="grid-content entry-content white label-content">
				<?php // Custom Title
				if ( get_field("custom-title")  == true ) { ?>
					<h3 class="custom-title">
						<?php _e("Les ", "digitemis");?>
						<svg width="1em" height="1em" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M7.20001 4H8.66668V7.2H11.8667V8.66666H8.66668V11.8667H7.20001V8.66666H4V7.2H7.20001V4Z" fill="currentColor"/>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM8 15.5C12.1421 15.5 15.5 12.1421 15.5 8C15.5 3.85786 12.1421 0.5 8 0.5C3.85786 0.5 0.5 3.85786 0.5 8C0.5 12.1421 3.85786 15.5 8 15.5Z" fill="currentColor"/>
						</svg>
						<?php //_e("DIGITEMIS", "digitemis");?>
						<?php echo get_bloginfo( 'name' );?>
					</h3>
				<?php 
				} ?>
				<?php the_field("content");?>
			</div>
		</div>
	</div>
</div>
<?php endif;
?>
</section>

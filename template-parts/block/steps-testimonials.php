<?php
/**
* Block Name: Bloc steps testimonials
*/
?>
<section class="step-use-case steps-testimonials">
<?php
$items = get_field('items');
if ( !$items ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-format-status"></span><br>
		<b>Bloc Témoignage</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper bloc-vertical-spacing white-bg-trick">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="h1-like underline"><?php the_field('title');?></<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php 
		} ?>

		<div class="steps-use-case-loop">
		<?php foreach ($items as $key => $item) :?>
			<div class="steps-use-case-loop-item">
				<div class="steps-use-case-historic entry-content grid-content">
					<?php 
					if( $item['title'] ){ ?>
						<h3 class="small-content-title">
							<?php echo $item['title'];?><br>
						</h3>
					<?php 
					} ?>
					<?php echo $item['content'];?>					
				</div>
				<div class="steps-use-case-loop-item-illustration grid-illustration">
					<div class="steps-use-case-aside entry-content white aside-padding">
						<?php 
						if( $item['aside-title'] ){ ?>
							<h3 class="small-content-title white">
								<?php echo $item['aside-title'];?>
							</h3>
						<?php 
						} ?>

						<?php echo $item['aside-content'];?>	
					</div>
					<?php
						$image = $item['image'];
						$size = 'square';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size);
						}
					?>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>

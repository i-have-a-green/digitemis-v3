<?php
/**
* Block Name: Bloc sub service
*/
?>
<section class="sub-service">
<?php
$items = get_field('items');
if ( !$items ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-awards"></span><br>
		<b>Bloc Navigation Sous-service</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper bloc-vertical-spacing">
		<div class="sub-service-content-loop">
		<?php foreach ($items as $key => $item) :
			?>
			<div class="loop-item center">
			<?php 
			$link = $item['link'];
			if ($link) {
			?>

				<a href="<?php echo $link;?>" class="item-title hexagon-bg hexagon-variante">
					<div class="item-title-content">
							<?php
								$image = $item['picto'];
								$size = '80-80';
								if( $image ) {
									echo wp_get_attachment_image( $image, $size, "", array( "class" => "sub-service-img" ));
								}
							?>	
							<h3><?php echo $item['title'];?></h3>
					</div>
				</a>

			<?php	
			} else {
			?>
				<div  class="item-title hexagon-bg hexagon-variante">
				<div class="item-title-content">
						<?php
							$image = $item['picto'];
							$size = '80-80';
							if( $image ) {
								echo wp_get_attachment_image( $image, $size, "", array( "class" => "sub-service-img" ));
							}
						?>	
						<h3><?php echo $item['title'];?></h3>
				</div>
			</div>
			<?php } ?>

				<div class="item-content">
					<?php echo $item['content']; /* sous titre dans ce contenu */?>
				</div>

				<?php 
				$link_label = $item['label'];
				if ( $link && $link_label ) { ?>
					<div class="item-link">
						<a href="<?php echo $item['link'];?>" class=" center button">
								<?php echo $item['label'];?>
						</a>
					</div>
				<?php } ?>

			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>

<?php
/**
* Block Name: Bloc Synthèse témoignage
*/
?>
<section class="synthesis wrapper-narrow-container full-width 
	<?php
	$blocIsWhite = false;
	if ( get_field('white-bg')  == true ) { 
		$blocIsWhite = true;
	}
	$isWhite = get_field('white-bg');
	if ( !$isWhite ) {
		echo 'blue-bg white';
	}?>
	">
<?php
$content = get_field('content');
if ( !$content ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-format-status"></span><br>
		<b>Bloc Synthèse Témoignage</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper-narrow center">

	

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title h2-like"><?php the_field('title');?><br>
				<span class="section-subtitle blue"><?php the_field('sub-title');?></span>
			</<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="small-content left">
			<?php the_field('content');?>
		</div>
		
		<?php if(get_field('libelle_du_lien') && get_field('lien')):?>
			<div class="right bloc-link">
				<?php 
					$isWhite = get_field('white-bg');
					if ( $isWhite ) { ?>
						<a href="<?php echo get_field('lien');?>" class="blue right-arrow uppercase">
							<?php echo get_field('libelle_du_lien');?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arrow-blue.svg" alt="" height="20" width="20">
					</a>
				<?php 
				} else { 
				?>
					<a href="<?php echo get_field('lien');?>" class="white right-arrow uppercase">
						<?php echo get_field('libelle_du_lien');?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arrow-white.svg" alt="" height="20" width="20">
					</a>
				<?php 
				} ?>
				
			</div>
		<?php endif;?>
	</div>
	<?php
endif;
?>
</section>

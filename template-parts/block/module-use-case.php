<?php
/**
* Block Name: Bloc module-use-case
*/
?>
<section class="module-use-case white full-width">
<?php
$items = get_field('items');
if ( !$items ) :
	?>
	<div style="text-align:center; color: white;" >
	<span class="dashicons dashicons-editor-ul"></span><br>
		<b>Bloc Module (UseCase)</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper element-graphique white vertical-bloc-spacing">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?> class="section-title underline underline-white center"><?php the_field('title');?></<?php echo(get_field('head_level'))?get_field('head_level'):'h2';?>>
		<?php } ?>

		<div class="items-module-case-loop">
		<?php foreach ($items as $key => $item) :
			?>
			<div class="items-module-case-loop-item">
				<div class="items-use-case-loop-item-content small-content">
					<h3 class="small-content-title light-blue">
						<?php echo $item['title'];?><br>
					</h3>
					<?php echo $item['content'];?>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>

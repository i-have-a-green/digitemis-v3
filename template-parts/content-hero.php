
<?php 
$image = get_field('image_banner');
if (wp_is_mobile()) {
    $size = 'page-hero-mobile';
}else {
    $size = 'page-hero';
}

if( $image ) :?>
    <style> 
        #page-<?php the_ID();?>::after {
			background-image: url(<?php echo wp_get_attachment_image_src( $image, $size )[0]; ?>);
        }
    </style>
<?php elseif ( has_post_thumbnail() ) : ?>
    <style> 
        #page-<?php the_ID();?>::after {
			background-image: url(<?php the_post_thumbnail_url($size); ?>);
        }
    </style>
<?php endif; ?>

<?php $allow_breadcrumb = get_field('allow_breadcrumb', 'option');
    if ($allow_breadcrumb) {
        wpBreadcrumb(); 
    }
?>
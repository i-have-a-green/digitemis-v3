<?php
/**
 * The template for displaying all single recrutement
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
    <article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Breadcrumb -->
        <div class="wrapper-narrow-container">
            <?php get_template_part( 'template-parts/content', 'hero' ); ?>
        </div>
    
        <!-- Recrutement -->
        <section class="above-hero">
            <div class="wrapper-narrow-container">
                <div class="wrapper-narrow white-bg">
                    <!-- Page-title -->
                        <h1 class="page-title underline small-margin center"><?php the_title();?></h1>
                        <!--<h2><?php //the_field( 'text-listing' ); ?></h2>-->
                </div>
            </div>
        </section>
                    
        <!-- Content -->
        <section class="above-hero wrapper-narrow white-bg">
            <div class="content-container">
                <?php the_content();?>
                <?php if(get_post_meta(get_the_id(), "type", true )):?>
                    <p>Type de contrat : <strong><?php echo get_post_meta(get_the_id(), "type", true );?> / <?php echo get_post_meta(get_the_id(), "type_short", true );?></strong></p>
                <?php endif;?>

                <p>
                    A
                    <strong><?php echo get_post_meta(get_the_id(), "city", true );?></strong>  
                    au
                    <strong><?php echo get_post_meta(get_the_id(), "address", true );?></strong> 
                </p>
    
                <?php if (get_post_meta(get_the_id(), "comp_desc", true )) {
                    ?>
                        <p><?php echo get_post_meta(get_the_id(), "comp_desc", true ); ?></p>
                    <?php
                }
                if (get_post_meta(get_the_id(), "job_desc", true )) {
                    ?>
                        <h3><?php _e('Description du poste','digitemis') ?></h3>
                        <p><?php echo get_post_meta(get_the_id(), "job_desc", true ); ?></p>
                    <?php
                }
                if (get_post_meta(get_the_id(), "guy_desc", true )) {
                    ?>
                        <h3><?php _e('Description du profil','digitemis') ?></h3>
                        <p><?php echo get_post_meta(get_the_id(), "guy_desc", true ); ?></p>
                    <?php
                } ?>
    
                <a target="_blank" data-trk-telechargement="telechargement-plaquette" href="<?php echo get_post_meta(get_the_id(), "apply", true );?>" class="button">Postuler</a>
            </div>
        </section>
        
        <!-- Recrutement form
        <section  class="wrapper-narrow-container narrow-form">
            <form class="form-grey-background has-section" name="contactRecrutement" id="contactRecrutement" action="#" method="POST">

                <h2 class="center h3-like full-width"><?php // _e("Prêt pour l'aventure DIGITEMIS ?", "digitemis");?></h2>

                <input type="hidden" name="honeyPot" value="">
                <input type="hidden" name="annonce" value="<?php // the_title();?>">
                <div class="half-width">
                    <label for="name"><?php // _e("Nom", "digitemis");?> *</label>
                    <input type="text" name="name" id="name" placeholder="Garnier" required value="">
                </div>
                <div class="half-width">
                    <label for="firstname"><?php // _e("Prénom", "digitemis");?> *</label>
                    <input type="text" name="firstname" id="firstname" placeholder="Alain" required value="">
                </div>

                <div class="half-width">
                    <label for="email"><?php // _e("Mail", "digitemis");?> *</label>
                    <input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="">
                </div>

                <div class="half-width">
                    <label for="phone"><?php // _e("Téléphone", "digitemis");?> *</label>
                    <input type="tel" name="phone" id="phone" placeholder="06 XX XX XX XX" required >
                </div>

                <div class="full-width">
                    <label for="poste"><?php // _e("Poste souhaité", "digitemis");?></label>
                    <input type="text" name="poste" id="poste" placeholder="<?php // the_title()?>" value="">
                </div>

                <div class=" full-width">
                    <label for="comment"><?php // _e("Message", "digitemis");?> *</label>
                    <textarea rows="5" name="comment" id="comment" placeholder="<?php // _e("Rédigez votre message …", "digitemis");?>" required value=""></textarea>
                </div>
                
                <div class="full-width">
                    <label for="cv">CV *</label>
                    <div class="upload-button">
                    <input type="file" id="cv" name="cv" accept="image/png, image/jpeg, application/pdf, application/msword " required>
                    <p>
                        <?php // _e("Déposez votre CV ici", "digitemis");?>
                        <i><?php // _e("Fichier PDF", "digitemis");?></i>
                    </p>
                    
                    </div>
                </div>

                <div class="full-width">
                    <label for="motiv"><?php // _e("Lettre de motivation", "digitemis");?> *</label>
                    <div class="upload-button">
                    <input type="file" id="motiv" name="motiv" accept="application/pdf" required>
                    <p>
                        <?php // _e("Déposez votre Lettre de motivation ici", "digitemis");?>
                        <i><?php // _e("Fichier PDF", "digitemis");?></i>
                    </p>
                    
                    </div>
                </div>

                <div id="ResponseAnchor" class="center full-width contain-button">
                    <input class="button button-purple" type="submit" id="sendMessage" value="Envoyer">
                <div id="ResponseMessage">
                    <?php // _e("Votre message a été envoyé !", 'IHAG');?>
                </div>

                <div class="full-width center">
                    <i class="formInfo">* <?php // _e("Informations obligatoires", "digitemis");?></i>
                    <i class="formInfo"><?php // _e("Les informations recueillies à partir de ce formulaire sont traitées par DIGITEMIS pour donner suite à votre candidature. Pour connaître vos droits et la politique de DIGITEMIS sur la protection des données,", "digitemis");?>
                        <a href="<?php // echo get_privacy_policy_url() ?>">
                        <?php // _e("Cliquez ici", "digitemis");?>
                        </a>
                    </i>
                </div>
            </form>
        </section>
        -->

    </article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();

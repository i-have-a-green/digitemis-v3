<?php
/**
 * The template for displaying all single service
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
	
<main id="main" class="light-grey-bg">
	<article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>
 
		<!-- Hero thumbnail -->
		<?php get_template_part( 'template-parts/content', 'hero-only' ); ?>

		<!-- Title -->
		<section class="wrapper-narrow-container above-hero">
			<?php  wpBreadcrumb(); ?>
			<div class="wrapper-narrow white-bg">
				<!-- Post-title -->
				<?php  the_title( '<h1 class="page-title center underline">', '</h1>' ); ?>
			</div>
		</section>

		<!-- Testimonial-content -->
		<section id="post-content" class="above-hero">
			<?php the_content();?>  
		</section>

		<section class="wrapper above-hero last-title">

			<h2 class="page-title underline small-margin center"><?php _e("Services complémentaires", 'digitemis');?></h2>

			<?php
			global $post;
			$services = get_posts( array(
				'post_type'	=> 'service',
				'posts_per_page' => 2,
				'post_status'    => 'publish',
				'include'		=> get_field("services_associes")
			) );
			
			?>
			<div class="post-container service-container bloc-vertical-spacing above-hero">
				
				<?php
					if ( $services ) {
						foreach ( $services as $post ) : ?>
							
						<div class="service-card">
							<div class="service-thumb white-bg">
								<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail('thumb-post'); 
								} else {
									$image = get_field('thumb-fallback', 'option');
									$size = 'thumb-post';
									if( $image ) {
									echo wp_get_attachment_image( $image, $size );
									}
								} ?>

							</div>
							<div class="service-legend">
								<h3><?php the_title()?></h3>
								<a href="<?php the_permalink()?>" title="<?php the_title()?>">
									<?php _e("Voir la prestation", 'digitemis');?>
								</a>
							</div>
						</div>
						
						<?php endforeach; 
					}
				?>
			</div><!-- .post-container -->
		</section>

	</article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();

<?php
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
{
    global $wp_styles;
    wp_enqueue_style('styles', get_stylesheet_directory_uri() . '/style.css');

    //wp_deregister_script('jquery');
    wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
    wp_enqueue_script('script');
    //wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script('script', 'resturl', array('resturl' => site_url() . '/wp-json/ihag/'));
    wp_localize_script( 'script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );
}
<?php

add_filter('upload_mimes', 'nbMymeTypes', 1, 1);
function nbMymeTypes($mime_types) {
	$mime_types['svg'] = 'image/svg+xml';
	$mime_types['doc'] = 'application/msword';
	$mime_types['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
	/*$mime_types['xls'] = 'application/vnd.ms-excel';
	$mime_types['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';*/
	return $mime_types;
}

/* Definitions des tailles d'images
@src https://developer.wordpress.org/reference/functions/add_image_size/
-----------------------------------------------------------------*/
if (function_exists('add_theme_support')) {

	// Qualité des images
	add_filter('jpeg_quality', function() {
		return (int)100;
	});

	// Add Thumbnail Theme Support
	add_theme_support('post-thumbnails');

	// Header Pages

	add_image_size('carrousel-bg', 1600, 1200, false);
	add_image_size('page-hero-mobile', 640, 212, true);
	add_image_size('page-hero', 1600, 530, true);
	add_image_size('thumb-post', 520, 346, true);
	//add_image_size('600-600', 600, 600, true);
	//add_image_size('square', 520, 520, true);
	add_image_size('free-height', 520);
	add_image_size('80-80', 80, 80, true);
	add_image_size('costumer', 250, 120, false);
	add_image_size('white-paper', 200, 250, true);
	//add_image_size('costumer', 250, 250, false);

}



/* Nettoie les medias uploadés
-----------------------------------------------------------------*/
add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€' ), $special_chars );
	return $special_chars;
}

function ihag_fix_svg_size_attributes( $out, $id ) {
    $image_url  = wp_get_attachment_url( $id );
    $file_ext   = pathinfo( $image_url, PATHINFO_EXTENSION );

    if ( is_admin() || 'svg' !== $file_ext ) {
        return false;
    }

    return array( $image_url, null, null, false );
}
add_filter( 'image_downsize', 'ihag_fix_svg_size_attributes', 10, 2 );

<?php
add_action( 'init', 'contact_custom_post_type');
function contact_custom_post_type() {
	register_post_type( 'ihag_form', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('Contact', 'ihag'), /* This is the Title of the Group */
				'singular_name' 	=> __('Contact', 'ihag'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'supports' 			=> array( 'title', 'editor','custom-fields')
	 	) /* end of options */
	);

	register_taxonomy(
		'type_form',
    	'ihag_form',
		array(
			'label'         => __( 'Formulaire' ),
			'hierarchical'  => true,
			'show_admin_column' => true,
			'query_var' => true,
      	)
  	);

}


/*
* traitement du post du form de Contact et du bloc formulaire
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'contactForm',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagContactForm',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'contactRecrutement',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormRecrutement',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'contactLivreBlanc',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormLivreBlanc',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'ihag', 'formRegistration', //webinar & training
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormRegistration',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'formNewsletter', 
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihagFormNewsletter',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	
});

function ihagFormNewsletter(WP_REST_Request $request){
	
	if ( check_nonce() ) {
	//if (empty($_POST['honeyPot'])) {
		if(addUserDoList (sanitize_email( $_POST['newletter_email'] ))){
			echo "Votre adresse a bien été enregistrée.";
		}
		else{
			//echo "Votre adresse existe dans notre base de données.";
		}
	//}		
	}
	return new WP_REST_Response( '', 200 );
	
}


function ihagContactForm(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {

			if(!is_company_email($_POST['email'])){
				return new WP_REST_Response( '', 520 );
			}


			global $wpdb;

			$upload_file_text = "";
			$attachments = array();

			$subject = __('Contact - Nouveau message depuis votre site web','ihag');
			
			$body = 'Prénom : '.sanitize_text_field(stripslashes($_POST['firstname']))."\r\n";
			$body .= 'Nom : '.sanitize_text_field(stripslashes($_POST['name']))."\r\n";
			$body .= 'email : '.sanitize_text_field(stripslashes($_POST['email']))."\r\n";
			$body .= 'Téléphone : '.sanitize_text_field(stripslashes($_POST['phone']))."\r\n";
			$body .= 'Entreprise : '.sanitize_text_field(stripslashes($_POST['company']))."\r\n";
			$body .= 'Code postal : '.sanitize_text_field(stripslashes($_POST['postal']))."\r\n";
			$body .= 'Secteur d\'activité : '.sanitize_text_field(stripslashes($_POST['activity']))."\r\n";
			$body .= 'Fonction : '.sanitize_text_field(stripslashes($_POST['function']))."\r\n";
			//$body .= 'Sujet : '.sanitize_text_field(stripslashes($_POST['subject']))."\r\n";
			$body .= 'Commentaire : '.sanitize_text_field(stripslashes($_POST['comment']))."\r\n";


			//$body .= $upload_file_text;
			
			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			wp_mail( get_option( 'admin_email'), $subject, $body, $headers, $attachments);
			
			$post['post_type']   = 'ihag_form';
			$post['post_status'] = 'publish';
			$post['post_title'] = 'Contact - '.sanitize_text_field($_POST['firstname']).' '.sanitize_text_field($_POST['name']).' ('.sanitize_text_field($_POST['company']).')';
			$post['post_content'] = $body;

			$post_id = wp_insert_post($post);

			$term_id = term_exists( 'Contact', 'type_form' );
			if( !$term_id ){
				$term_id = wp_insert_term( "Contact", "type_form");
			}
			wp_set_post_terms( $post_id, $term_id, "type_form" );
			add_post_meta( $post_id, 'type_form', "contact", true ); 

			add_post_meta( $post_id, 'firstname', sanitize_text_field($_POST['firstname']), true ); 
			add_post_meta( $post_id, 'name', sanitize_text_field($_POST['name']), true );
			add_post_meta( $post_id, 'email', sanitize_text_field($_POST['email']), true );
			add_post_meta( $post_id, 'phone', sanitize_text_field($_POST['phone']), true );
			add_post_meta( $post_id, 'company', sanitize_text_field($_POST['company']), true );
			add_post_meta( $post_id, 'postal', sanitize_text_field($_POST['postal']), true );
			add_post_meta( $post_id, 'activity', sanitize_text_field($_POST['activity']), true );
			add_post_meta( $post_id, 'function', sanitize_text_field($_POST['function']), true );
			add_post_meta( $post_id, 'comment', sanitize_text_field($_POST['comment']), true );

		}
		return new WP_REST_Response( '', 200 );
	}
}

function ihagFormLivreBlanc(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {


			global $wpdb;

			$upload_file_text = "";
			$attachments = array();

			$subject = __('Livre blanc - Nouveau message depuis votre site web','ihag');
			
			$body = 'Prénom : '.sanitize_text_field(stripslashes($_POST['firstname']))."\r\n";
			$body .= 'Nom : '.sanitize_text_field(stripslashes($_POST['name']))."\r\n";
			$body .= 'email : '.sanitize_text_field(stripslashes($_POST['email']))."\r\n";
			$body .= 'Téléphone : '.sanitize_text_field(stripslashes($_POST['phone']))."\r\n";
			$body .= 'Entreprise : '.sanitize_text_field(stripslashes($_POST['company']))."\r\n";
			$body .= 'Secteur d\'activité : '.sanitize_text_field(stripslashes($_POST['activity']))."\r\n";
			$body .= 'Fonction : '.sanitize_text_field(stripslashes($_POST['function']))."\r\n";

			if(empty($_POST['NewsletterCheckbox'])){
				$body .= "Newsletter : inscription\r\n";
			}
			else{
				$body .= "Newsletter : non\r\n";
			}
			if(empty($_POST['recontactCheckbox'])){
				$body .= "Recontact : OK\r\n";
			}
			else{
				$body .= "Recontact : non\r\n";
			}
			
			
			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			wp_mail( get_option( 'admin_email'), $subject, $body, $headers, $attachments);
			
			$post['post_type']   = 'ihag_form';
			$post['post_status'] = 'publish';
			$post['post_title'] = "Livre blanc - ".sanitize_text_field($_POST['firstname']).' '.sanitize_text_field($_POST['name']).' ('.sanitize_text_field($_POST['company']).')';
			$post['post_content'] = $body;

			$post_id = wp_insert_post($post);

			$term_id = term_exists( 'Livre blanc', 'type_form' );
			if( !$term_id ){
				$term_id = wp_insert_term( "Livre blanc", "type_form");
			}
			wp_set_post_terms( $post_id, $term_id, "type_form" );
			add_post_meta( $post_id, 'type_form', "livreblanc", true );

			add_post_meta( $post_id, 'firstname', sanitize_text_field($_POST['firstname']), true ); 
			add_post_meta( $post_id, 'name', sanitize_text_field($_POST['name']), true );
			add_post_meta( $post_id, 'email', sanitize_text_field($_POST['email']), true );
			add_post_meta( $post_id, 'phone', sanitize_text_field($_POST['phone']), true );
			add_post_meta( $post_id, 'company', sanitize_text_field($_POST['company']), true );
			add_post_meta( $post_id, 'activity', sanitize_text_field($_POST['activity']), true );
			add_post_meta( $post_id, 'function', sanitize_text_field($_POST['function']), true );
			if(empty($_POST['NewsletterCheckbox'])){
				add_post_meta( $post_id, 'newsletter', "inscription", true );
			}
			else{
				add_post_meta( $post_id, 'newsletter', "non", true );
			}
			if(empty($_POST['recontactCheckbox'])){
				add_post_meta( $post_id, 'recontact', "OK", true );
			}
			else{
				add_post_meta( $post_id, 'recontact', "non", true );
			}

			$attachments = array( get_attached_file( get_field("livre_blanc", "option")));
			error_log(get_attached_file( get_field("livre_blanc", "option")));
		
			$body = __('Bonjour,
	Nous avons le plaisir de vous envoyer ce livre blanc
	Cordialement','ihag');
			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			wp_mail( sanitize_text_field($_POST['email']), __("Votre demande de livre blanc",'ihag'), $body, $headers, $attachments);

			if(empty($_POST['NewsletterCheckbox'])){
				addUserDoList (sanitize_email( $_POST['email'] ));
			}
		}
		return new WP_REST_Response( '', 200 );
	}
}


function ihagFormRecrutement(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {

			global $wpdb;

			$upload_file_text = "";
			$attachments = array();
			if(isset($_FILES['cv'])){
				$maintenant = date("d-m-Y_H:i:s");
				$upload_dir   = wp_upload_dir();
				$uploaddirimg = $upload_dir['basedir'].'/cv/';
				mkdir($uploaddirimg, 0755);
				$uploadfile = $uploaddirimg . $maintenant . '-'.basename($_FILES['cv']['name']);
				if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadfile)) {
					array_push($attachments, $uploadfile);
					$upload_file_text .= "CV : ".$upload_dir['baseurl'].'/cv/' . $maintenant . '-'.basename($_FILES['cv']['name'])."\r\n";
				}
			}

			if(isset($_FILES['motiv'])){
				$maintenant = date("d-m-Y_H:i:s");
				$upload_dir   = wp_upload_dir();
				$uploaddirimg = $upload_dir['basedir'].'/cv/';
				mkdir($uploaddirimg, 0755);
				$uploadfile = $uploaddirimg . $maintenant . '-'.basename($_FILES['motiv']['name']);
				if (move_uploaded_file($_FILES['motiv']['tmp_name'], $uploadfile)) {
					array_push($attachments, $uploadfile);
					$upload_file_text .= "Motivation : ".$upload_dir['baseurl'].'/cv/' . $maintenant . '-'.basename($_FILES['motiv']['name'])."\r\n";
				}
			}

			$subject = __('Recrutement - Nouveau message depuis votre site web','ihag');
			
			$body = 'Prénom : '.sanitize_text_field(stripslashes($_POST['firstname']))."\r\n";
			$body .= 'Nom : '.sanitize_text_field(stripslashes($_POST['name']))."\r\n";
			$body .= 'email : '.sanitize_text_field(stripslashes($_POST['email']))."\r\n";
			$body .= 'Téléphone : '.sanitize_text_field(stripslashes($_POST['phone']))."\r\n";
			$body .= 'Poste : '.sanitize_text_field(stripslashes($_POST['poste']))."\r\n";
			$body .= 'Annonce : '.sanitize_text_field(stripslashes($_POST['annonce']))."\r\n";
			$body .= 'Commentaire : '.sanitize_text_field(stripslashes($_POST['comment']))."\r\n";
			$body .= $upload_file_text;

			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			wp_mail( get_option( 'admin_email'), $subject, $body, $headers, $attachments);
			
			$post['post_type']   = 'ihag_form';
			$post['post_status'] = 'publish';
			$post['post_title'] = 'CV - '.sanitize_text_field($_POST['firstname']).' '.sanitize_text_field($_POST['name']).' ('.sanitize_text_field($_POST['annonce']).')';
			$post['post_content'] = $body;

			$post_id = wp_insert_post($post);

			$term_id = term_exists( 'CV', 'type_form' );
			if( !$term_id ){
				$term_id = wp_insert_term( "CV", "type_form");
			}
			wp_set_post_terms( $post_id, $term_id, "type_form" );
			add_post_meta( $post_id, 'type_form', "recrutement", true );

			add_post_meta( $post_id, 'firstname', sanitize_text_field($_POST['firstname']), true ); 
			add_post_meta( $post_id, 'name', sanitize_text_field($_POST['name']), true );
			add_post_meta( $post_id, 'email', sanitize_text_field($_POST['email']), true );
			add_post_meta( $post_id, 'phone', sanitize_text_field($_POST['phone']), true );
			add_post_meta( $post_id, 'poste', sanitize_text_field($_POST['poste']), true );
			add_post_meta( $post_id, 'annonce', sanitize_text_field($_POST['annonce']), true );
			add_post_meta( $post_id, 'comment', sanitize_text_field($_POST['comment']), true );
			add_post_meta( $post_id, 'CV', $upload_file_text, true );

			
		}
		return new WP_REST_Response( '', 200 );
	}
}

function ihagFormRegistration(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {

			global $wpdb;

			$upload_file_text = "";
			
			$subject = __($_POST['subject'].' - Nouveau message depuis votre site web','ihag');
			
			$body = sanitize_text_field(stripslashes($_POST['title']))."\r\n";
			$body .= 'Prénom : '.sanitize_text_field(stripslashes($_POST['firstname']))."\r\n";
			$body .= 'Nom : '.sanitize_text_field(stripslashes($_POST['name']))."\r\n";
			$body .= 'email : '.sanitize_text_field(stripslashes($_POST['email']))."\r\n";
			$body .= 'Entreprise : '.sanitize_text_field(stripslashes($_POST['company']))."\r\n";
			$body .= 'Activité : '.sanitize_text_field(stripslashes($_POST['activity']))."\r\n";
			$body .= 'Code Postal : '.sanitize_text_field(stripslashes($_POST['postal']))."\r\n";
			$body .= 'Session : '.sanitize_text_field(stripslashes($_POST['session']))."\r\n";
			$body .= 'Acquisition : '.sanitize_text_field(stripslashes($_POST['acquisition']))."\r\n";
			$body .= 'Sujet : '.sanitize_text_field(stripslashes($_POST['subject']))."\r\n";

			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			wp_mail( get_option( 'admin_email'), $subject, $body, $headers, $attachments);
			
			$post['post_type']   = 'ihag_form';
			$post['post_status'] = 'publish';
			//$post['post_title'] = $_POST['subject'].' - '.sanitize_text_field($_POST['firstname']).' '.sanitize_text_field($_POST['name']).' ('.sanitize_text_field($_POST['title']).')';
			$post['post_title'] = sanitize_text_field($_POST['firstname']).' '.sanitize_text_field($_POST['name']).' ('.sanitize_text_field($_POST['title']).')';
			$post['post_content'] = $body;

			$post_id = wp_insert_post($post);

			$term_id = term_exists( $_POST['subject'], 'type_form' );
			if( !$term_id ){
				$term_id = wp_insert_term( $_POST['subject'], "type_form");
			}
			wp_set_post_terms( $post_id, $term_id, "type_form" );
			add_post_meta( $post_id, 'type_form', strtolower($_POST['subject']), true );

			add_post_meta( $post_id, 'firstname', sanitize_text_field($_POST['firstname']), true ); 
			add_post_meta( $post_id, 'name', sanitize_text_field($_POST['name']), true );
			add_post_meta( $post_id, 'email', sanitize_text_field($_POST['email']), true );
			add_post_meta( $post_id, 'company', sanitize_text_field($_POST['company']), true );
			add_post_meta( $post_id, 'postal', sanitize_text_field($_POST['postal']), true );
			add_post_meta( $post_id, 'session', sanitize_text_field($_POST['session']), true );
			add_post_meta( $post_id, 'acquisition', sanitize_text_field($_POST['acquisition']), true );
			add_post_meta( $post_id, 'subject', sanitize_text_field($_POST['subject']), true );
			add_post_meta( $post_id, 'title', sanitize_text_field($_POST['title']), true );
			
			
			
		}
		return new WP_REST_Response( '', 200 );
	}
}


// wp_dashboard_setup is the action hook
//add_action('wp_dashboard_setup', 'ihag_dashboard_contact');
function ihag_dashboard_contact() {
    wp_add_dashboard_widget('ihag_dashboard_contact', 'Formulaire','ihag_dashboard_contact_export');
}
function ihag_dashboard_contact_export(){
	?>
	<ul>
        <?php
		$args = array( 
			'post_type' => 'ihag_form',
			'posts_per_page' => 10, 
			/*'tax_query' => array(
				array(
					'taxonomy' => "type_form",
					'field' => 'term_id',
					'terms' => $term_id
				)
			)*/
		);
        $myposts = get_posts( $args );
		foreach ( $myposts as $post ) :
            echo '<li><a href="'.admin_url( 'post.php?post='.$post->ID.'&action=edit').'">'.date_i18n(get_option('date_format'),strtotime($post->post_date)).' - '.$post->post_title.'</a></li>';
        endforeach;
        ?>
    </ul>
    <p>
    <a href="?report=ihag_export&taxo=webinar" class="button">Export Webinar</a>
	<a href="?report=ihag_export&taxo=training" class="button">Export Formation</a>
	<a href="?report=ihag_export&taxo=recrutement" class="button">Export Recrutement</a>
	<a href="?report=ihag_export&taxo=livreblanc" class="button">Export Livre Blanc</a>
	<a href="?report=ihag_export&taxo=contact" class="button">Export Contact</a>
    </p>
	<?php
}
class ihag_CSVExport
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'ihag_export' && isset($_GET['taxo']) && $_GET['taxo'] == 'webinar')
        {
        	$this->ihag_export_webinar();
		}
		elseif(isset($_GET['report']) && $_GET['report'] == 'ihag_export' && isset($_GET['taxo']) && $_GET['taxo'] == 'livreblanc')
        {
        	$this->ihag_export_livre_blanc();
		}
		elseif(isset($_GET['report']) && $_GET['report'] == 'ihag_export' && isset($_GET['taxo']) && $_GET['taxo'] == 'recrutement')
        {
        	$this->ihag_export_recrutement();
		}
		elseif(isset($_GET['report']) && $_GET['report'] == 'ihag_export' && isset($_GET['taxo']) && $_GET['taxo'] == 'training')
        {
        	$this->ihag_export_registration();
		}
		elseif(isset($_GET['report']) && $_GET['report'] == 'ihag_export' && isset($_GET['taxo']) && $_GET['taxo'] == 'contact')
        {
        	$this->ihag_export_contact();
        }
    }
	public function ihag_export_webinar(){
		global $wpdb, $post;
		$csv_fields=array();
		$csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Entreprise';
		$csv_fields[] = 'Acquisition';
		$csv_fields[] = 'Session';
		$csv_fields[] = 'Webinar';
        $output_filename = "webinar_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
		fputcsv( $output_handle, $csv_fields,";" );
		
		$args = array( 
			'posts_per_page' => -1, 
			'post_type' => 'ihag_form',
			'meta_key'   => 'type_form',
   			'meta_value' => 'webinar'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
			$tab_data = array( 
				get_post_meta( $post->ID, 'firstname',true),
				get_post_meta( $post->ID, 'name',true),
				get_post_meta( $post->ID, 'email',true),
				get_post_meta( $post->ID, 'company',true),
				get_post_meta( $post->ID, 'acquisition',true),
				get_post_meta( $post->ID, 'session',true),
				get_post_meta( $post->ID, 'title',true),
			);
			
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
		wp_reset_postdata();
        fclose( $output_handle );
		exit();
	}

	public function ihag_export_recrutement(){
		global $wpdb, $post;
		$csv_fields=array();
		$csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Tel';
		$csv_fields[] = 'Poste';
		$csv_fields[] = 'Annonce';
		$csv_fields[] = 'Commentaire';
		$csv_fields[] = 'CV';
        $output_filename = "recrutement_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
		fputcsv( $output_handle, $csv_fields,";" );
		
		$term_id = term_exists( "CV", 'type_form' );
		$args = array( 
			'posts_per_page' => -1, 
			'post_type' => 'ihag_form',
			'meta_key'   => 'type_form',
   			'meta_value' => 'recrutement'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
			$tab_data = array( 
				get_post_meta( $post->ID, 'firstname',true),
				get_post_meta( $post->ID, 'name',true),
				get_post_meta( $post->ID, 'email',true),
				get_post_meta( $post->ID, 'phone',true),
				get_post_meta( $post->ID, 'poste',true),
				get_post_meta( $post->ID, 'annonce',true),
				get_post_meta( $post->ID, 'comment',true),
				get_post_meta( $post->ID, 'CV',true),
			);
			
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
		wp_reset_postdata();
        fclose( $output_handle );
		exit();
	}

	public function ihag_export_livre_blanc(){
		global $wpdb, $post;
		$csv_fields=array();
		$csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Tel';
		$csv_fields[] = 'Entreprise';
		$csv_fields[] = "Secteur d'activité";
		$csv_fields[] = 'Fonction';
		$csv_fields[] = 'Newsletter';
		$csv_fields[] = 'Recontact';
        $output_filename = "LivreBlanc_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
		fputcsv( $output_handle, $csv_fields,";" );
		
		$term_id = term_exists( "Livre blanc", 'type_form' );
		$args = array( 
			'posts_per_page' => -1, 
			'post_type' => 'ihag_form',
			'meta_key'   => 'type_form',
   			'meta_value' => 'livreblanc'
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
			$tab_data = array( 
				get_post_meta( $post->ID, 'firstname',true),
				get_post_meta( $post->ID, 'name',true),
				get_post_meta( $post->ID, 'email',true),
				get_post_meta( $post->ID, 'phone',true),
				get_post_meta( $post->ID, 'company',true),
				get_post_meta( $post->ID, 'activity',true),
				get_post_meta( $post->ID, 'function',true),
				get_post_meta( $post->ID, 'newsletter',true),
				get_post_meta( $post->ID, 'recontact',true),
			);
			
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
		wp_reset_postdata();
        fclose( $output_handle );
		exit();
	}

	public function ihag_export_registration(){
		global $wpdb, $post;
		
		$csv_fields=array();
		$csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
		$csv_fields[] = 'Email';
		/*$csv_fields[] = 'Téléphone';*/
		$csv_fields[] = 'Entreprise';
		$csv_fields[] = 'Activité';
		//$csv_fields[] = 'Fonction';
		$csv_fields[] = 'Code postal';
		$csv_fields[] = 'Acquisition';
        $output_filename = $_GET['taxo']."_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
		fputcsv( $output_handle, $csv_fields,";" );
		
		
		$args = array( 
			'posts_per_page' => -1, 
			'post_type' => 'ihag_form',
			'meta_key'   => 'type_form',
   			'meta_value' => $_GET['taxo']
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
		
			$tab_data = array( 
				get_post_meta( $post->ID, 'firstname',true),
				get_post_meta( $post->ID, 'name',true),
				get_post_meta( $post->ID, 'email',true),
				//get_post_meta( $post->ID, 'phone',true),
				get_post_meta( $post->ID, 'company',true),
				get_post_meta( $post->ID, 'activity',true),
				get_post_meta( $post->ID, 'postal',true),				
				get_post_meta( $post->ID, 'acquisition',true),
				
			);
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
		wp_reset_postdata();
        fclose( $output_handle );
		exit();
	}

	public function ihag_export_contact(){
		global $wpdb, $post;
		
		$csv_fields=array();
		$csv_fields[] = 'Prénom';
        $csv_fields[] = 'Nom';
		$csv_fields[] = 'Email';
		$csv_fields[] = 'Téléphone';
		$csv_fields[] = 'Entreprise';
		$csv_fields[] = 'Code Postal';
		$csv_fields[] = 'Activité';
		$csv_fields[] = 'Fonction';
		$csv_fields[] = 'Commentaires';
        $output_filename = $_GET['taxo']."_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
		fputcsv( $output_handle, $csv_fields,";" );
		
		
		$args = array( 
			'posts_per_page' => -1, 
			'post_type' => 'ihag_form',
			'meta_key'   => 'type_form',
   			'meta_value' => $_GET['taxo']
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
		
			$tab_data = array( 
				get_post_meta( $post->ID, 'firstname',true),
				get_post_meta( $post->ID, 'name',true),
				get_post_meta( $post->ID, 'email',true),
				get_post_meta( $post->ID, 'phone',true),
				get_post_meta( $post->ID, 'company',true),
				get_post_meta( $post->ID, 'postal',true),				
				get_post_meta( $post->ID, 'activity',true),
				get_post_meta( $post->ID, 'function',true),
				get_post_meta( $post->ID, 'comment',true),				
			);
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
		wp_reset_postdata();
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new ihag_CSVExport();


function addUserMailChimp($email){
	// API to mailchimp ########################################################
	$list_id = '8867973e16';
	$authToken = '6619bd30de26a1a6056b11fce7747071-us5';
	// The data to send to the API

	$postData = array(
		"email_address" => $email, 
		"status" => "subscribed", 
		/*"merge_fields" => array(
		"FNAME"=> $_POST["name"],
		"PHONE"=> $_POST["phone"])*/
	);

	// Setup cURL
	$ch = curl_init('https://us5.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
	curl_setopt_array($ch, array(
		CURLOPT_POST => TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			'Authorization: apikey '.$authToken,
			'Content-Type: application/json'
		),
		CURLOPT_POSTFIELDS => json_encode($postData)
	));
	// Send the request
//	$response = curl_exec($ch);
	$result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
	return ($httpCode == 200);
}

function addUserDoList($email){
	


	//$curl = curl_init();
	/*curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://apiv9.dolist.net/v1/fields?AccountID=6513",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  //CURLOPT_POSTFIELDS => "{\"Contact\":{\"FieldList\":[{\"ID\":2000,\"Value\":\"$email\"}]}}",
	  CURLOPT_HTTPHEADER => array(
		"accept: application/json",
    	"x-api-key: c4f9f8c6e4da723d8ccfaaa85eed9dedc092e39712ecdd0e48f396ce5ad350a1"
	  ),
	));*/
	
	
	/*curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://apiv9.dolist.net/v1/contacts?AccountID=6513",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"Contact\":{\"FieldList\":[{\"ID\":7,\"Value\":\"$email\"}]}}",
	  CURLOPT_HTTPHEADER => array(
		"accept: application/json",
		"content-type: application/json",
		"x-api-key: c4f9f8c6e4da723d8ccfaaa85eed9dedc092e39712ecdd0e48f396ce5ad350a1"
	  ),
	));
	
	$response = curl_exec($curl);
	$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	$err = curl_error($curl);
	
	curl_close($curl);
	
	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  return $httpCode;
	}*/


	$subject = __('Inscription newsletter','ihag');
			
	$body = 'Email à ajouter à la newsletter : '.$email."\r\n";

	$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'ihag') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
	wp_mail( get_option( 'admin_email'), $subject, $body, $headers);
	return true;
	
}
<?php
//API Jobaffinity


add_action('admin_menu', 'customPageImport');
 
function customPageImport() {
    add_submenu_page(
        'tools.php',
        'jobImport',
        'jobImport',
        'manage_options',
        'importjobs',
        'customMenuImport' );
}

function customMenuImport() {
    ?>
    <div class="wrap">
        <h1>Importer les annonces JobAffinity</h1>
        <a href="?report=importJobs" class="button">Importer</a>
    <?php
}

add_action('init', function() {
    if(isset($_GET['report']) && $_GET['report'] == 'importJobs'){
        import_jobs();
    }
});

/* function TimerDesactivate() {
    wp_clear_scheduled_hook( 'TimerDesactivate' );
}
 
add_action('init', function() {
    add_action( 'TimeimportJobs', 'importJobs' );
    register_deactivation_hook( __FILE__, 'TimerDesactivate' );
 
    if (! wp_next_scheduled ( 'TimeimportJobs' )) {
        wp_schedule_event( time(), 'daily', 'TimeimportJobs' );
    }

    register_deactivation_hook( __FILE__, 'my_deactivation' );
 
    function my_deactivation() {
        wp_clear_scheduled_hook( 'TimeimportJobs' );
    }

}); */
if ( ! wp_next_scheduled( 'digitemis_jobs' ) ) {
    wp_schedule_event( time(), 'daily', 'digitemis_jobs' );
}

add_action( 'digitemis_jobs', 'import_jobs' );
function import_jobs(){

    // APPEL API
    $ch = curl_init();
    //$url = "https://jobaffinity.fr/feed/Ztvz0HBzboU4wYb/json";
    $url = get_field('url_job_affinity','options');
    error_log($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);	
    $r = curl_exec($ch);
    //$info = curl_getinfo($ch);
    $r = json_decode( $r );

    // DELETE POST TYPE
    $joblists = get_posts( array('numberposts' => -1 ,'post_type' => 'recrutement') );
    foreach ( $joblists as $joblist ) {
        wp_delete_post( $joblist->ID, true); // Set to False if you want to send them to Trash.
    } 


    // BOUCLE SELON NOMBRE DE JOBS
    foreach ($r->jobs as $job) {
        $post = array();
        $post['post_type']   = 'recrutement';
        $post['post_status'] = 'publish';
        $post['post_title'] = $job->title;
        //$post['post_content'] = ;
        $post_id = wp_insert_post( $post, true );
        
        $htmlEls =  array(
                        'a' => array(
                            'href' => array(),
                            'title' => array()
                        ),
                        'p' => array(),
                        'br' => array(),
                        'em' => array(),
                        'strong' => array(),
                    );
                    
        // Intituler du poste pour liste
        update_post_meta($post_id, 'name', sanitize_text_field($job->title));

        // Type du poste
        update_post_meta($post_id, 'type', sanitize_text_field($job->contract_type_name));
        update_post_meta($post_id, 'type_short', sanitize_text_field($job->contract_type_abbreviation));

        //Description du poste & profil & company
        update_post_meta($post_id, 'job_desc',wp_kses($job->position_description,$htmlEls));
        update_post_meta($post_id, 'guy_desc', wp_kses($job->profile_description,$htmlEls));
        update_post_meta($post_id, 'comp_desc', wp_kses($job->employer_description,$htmlEls));

        // Localisation du poste
        update_post_meta($post_id, 'city', sanitize_text_field($job->location));
        update_post_meta($post_id, 'address', sanitize_text_field($job->address));


        update_post_meta($post_id, 'apply', sanitize_text_field($job->apply_web_url));

    }
    flush_rewrite_rules();
}        


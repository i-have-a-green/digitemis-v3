<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'digitemis',
                'title' => __( 'Digitemis', 'digitemis' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories_all', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(
            array(
                'name'				    => 'carrousel-home',
                'title'				    => __('Carroussel - Page d\'accueil'),
                'description'		    => __('Fait défiler plusieurs slides sur la page d\'accueil'),
                'placeholder'		    => __('Carroussel - Page d\'accueil'),
                'render_template'	    => 'template-parts/block/carrousel-home.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'slides',
                'keywords'			    => array('home', 'carroussel', 'slider', 'slideshow'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'header-image',
                'title'				    => __('Header image - Make It Safe'),
                'description'		    => __('Header avec une image, similaire au caroussel de la page d\'accueil. Prévu pour la page MAke it safe'),
                'placeholder'		    => __('Header image - Make It Safe'),
                'render_template'	    => 'template-parts/block/header-image.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('header', 'make', 'it', 'safe', 'slider', 'slideshow'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'chapo',
                'title'				    => __('Chapô'),
                'description'		    => __('Entête composée d\'un titre, sous-titre et texte'),
                'placeholder'		    => __('Chapô'),
                'render_template'	    => 'template-parts/block/chapo.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'editor-paragraph',
                'keywords'			    => array('chapo', 'header', 'titre'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'last-posts',
                'title'				    => __('Derniers articles'),
                'description'		    => __('Affiche les 2 derniers rticles du blog'),
                'placeholder'		    => __('Derniers articles'),
                'render_template'	    => 'template-parts/block/last-posts.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'admin-post',
                'keywords'			    => array('actualité', 'derniers', 'articles', 'post', 'blog'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'content-image',
                'title'				    => __('Contenu et image'),
                'description'		    => __('Bloc standard pour afficher texte et image côte-à-côte'),
                'placeholder'		    => __('Contenu et image'),
                'render_template'	    => 'template-parts/block/content-image.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('contenu', 'image', 'paragraphe', 'content'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'content-image-light',
                'title'				    => __('Contenu et image - Light'),
                'description'		    => __('Bloc simple pour afficher texte + image'),
                'placeholder'		    => __('Contenu et image - Light'),
                'render_template'	    => 'template-parts/block/content-image-light.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('contenu', 'image', 'paragraphe', 'content'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'steps-line',
                'title'				    => __('Visuel 4 étapes (Responsive)'),
                'description'		    => __('Image responsive : Version desktop et version mobile'),
                'placeholder'		    => __('Visuel 4 étapes (Responsive)'),
                'render_template'	    => 'template-parts/block/steps-line.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('4stepsLine', '4', 'étapes', 'visuel', 'responsive'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'testimonial',
                'title'				    => __('Bannière (Témoignage)'),
                'description'		    => __('Lein vers un témoignage, avec une image en arrière-plan'),
                'placeholder'		    => __('Bannière (Témoignage)'),
                'render_template'	    => 'template-parts/block/testimonial.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'admin-links',
                'keywords'			    => array('testimonial', 'temoignage', 'témoignage', 'banniere', 'bannière', 'banner'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'costumers',
                'title'				    => __('Logos clients'),
                'description'		    => __('Affiche les logos des clients de Digitemis'),
                'placeholder'		    => __('Logos clients'),
                'render_template'	    => 'template-parts/block/costumers.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'images-alt',
                'keywords'			    => array('costumer', 'costumers', 'client', 'logo'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'white-paper',
                'title'				    => __('Livre blanc'),
                'description'		    => __('Livre blanc'),
                'placeholder'		    => __('Livre blanc'),
                'render_template'	    => 'template-parts/block/white-paper.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('Livre blanc', 'white-paper'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'way',
                'title'				    => __('Étapes'),
                'description'		    => __('Liste de plusieurs étapes (avec titre, texte et pictogramme)'),
                'placeholder'		    => __('Étapes'),
                'render_template'	    => 'template-parts/block/way.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'editor-ul',
                'keywords'			    => array('way', 'étapes', 'usecase', 'contenu','alterné'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'cross-nav',
                'title'				    => __('Navigation croisée'),
                'description'		    => __('Affiche plusieurs liens (icônes + texte) sur une seule rangée'),
                'placeholder'		    => __('Navigation croisée'),
                'render_template'	    => 'template-parts/block/cross-nav.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'dashicons-admin-links',
                'keywords'			    => array('navigation', 'lien','croisé'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'button',
                'title'				    => __('Bouton'),
                'description'		    => __('Ajoute un bouton dans la page'),
                'placeholder'		    => __('Bouton'),
                'render_template'	    => 'template-parts/block/button.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'admin-links',
                'keywords'			    => array('button', 'bouton','croisé'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'step-use-case',
                'title'				    => __('Étapes (UseCase)'),
                'description'		    => __('Liste de plusieurs étapes (avec titre, texte, pictogramme et visuel)'),
                'placeholder'		    => __('Étapes (UseCase)'),
                'render_template'	    => 'template-parts/block/step-use-case.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-editor-ul',
                'keywords'			    => array('step-use-case', 'contenu','alterné', 'usecase'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'abstract-use-case',
                'title'				    => __('Synthèse (UseCase)'),
                'description'		    => __('Synthèse visuelle : plusieurs items (icônes + texte) sur une seule rangée'),
                'placeholder'		    => __('Synthèse (UseCase)'),
                'render_template'	    => 'template-parts/block/abstract-use-case.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-editor-ul',
                'keywords'			    => array('Synthèse UseCase', 'Synthèse','abstract', 'usecase'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'module-use-case',
                'title'				    => __('Module UseCase'),
                'description'		    => __('Liste de modules (page Solutions)'),
                'placeholder'		    => __('Module UseCase'),
                'render_template'	    => 'template-parts/block/module-use-case.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'editor-ul',
                'keywords'			    => array('Module UseCase', 'module','usecase'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'pre-footer',
                'title'				    => __('Pré-footer Témoignage'),
                'description'		    => __('Lien un témoignage, avec un arrière-plan bleu | À placer à la fin de la page'),
                'placeholder'		    => __('Pré-footer Témoignage'),
                'render_template'	    => 'template-parts/block/pre-footer.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'admin-links',
                'keywords'			    => array('Pré footer', 'témoignage'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'steps-company',
                'title'				    => __('Historique'),
                'description'		    => __('Montre les années importantes de l\'entreprise'),
                'placeholder'		    => __('Historique'),
                'render_template'	    => 'template-parts/block/steps-company.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'clock',
                'keywords'			    => array('Historique', 'steps', 'entreprise'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        acf_register_block_type(
            array(
                'name'				    => 'values',
                'title'				    => __('Values'),
                'description'		    => __('Values'),
                'placeholder'		    => __('Values'),
                'render_template'	    => 'template-parts/block/values.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-image',
                'keywords'			    => array('Values', ),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'steps-testimonials',
                'title'				    => __('Témoignage'),
                'description'		    => __('Affiche les informations du témoignage (titre, texte, image, focus infos)'),
                'placeholder'		    => __('Témoignage'),
                'render_template'	    => 'template-parts/block/steps-testimonials.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-status',
                'keywords'			    => array('Témoignages', ),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'recrutement',
                'title'				    => __('Recrutement listing'),
                'description'		    => __('Affiche la liste de toutes les offres d\'emplois'),
                'placeholder'		    => __('Recrutement listing'),
                'render_template'	    => 'template-parts/block/recrutement.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'megaphone',
                'keywords'			    => array('Recrutement', 'listing', 'offres', 'emploi'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'training',
                'title'				    => __('Formation : Contenu'),
                'description'		    => __('Aperçu de la formation (avec Titre, texte, image et texte sur fond bleu)'),
                'placeholder'		    => __('Formation : Contenu'),
                'render_template'	    => 'template-parts/block/training.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'welcome-learn-more',
                'keywords'			    => array('training', 'formation', 'fiche', 'détails', 'informations', 'technique', 'caractéristique'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'training-label',
                'title'				    => __('Formation : Focus Label'),
                'description'		    => __('Focus sur le point fort de la formation, avec ume image ( par exemple : le label CNIL)'),
                'placeholder'		    => __('Formation : Focus Label'),
                'render_template'	    => 'template-parts/block/training-label.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'shield',
                'keywords'			    => array('Label', 'cnil', 'formation'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'training-content',
                'title'				    => __('Formation : Fiche technique'),
                'description'		    => __('Caracétristiques détaillées de la formation | Arrière-plan gris'),
                'placeholder'		    => __('Formation : Fiche technique'),
                'render_template'	    => 'template-parts/block/training-content.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'welcome-learn-more',
                'keywords'			    => array('Contenu', 'formation', 'fact', 'fiche', 'pratique'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'sub-service',
                'title'				    => __('Navigation Sous-service'),
                'description'		    => __('Affiche plusieurs sous-services (icônes + texte) sur une seule rangée'),
                'placeholder'		    => __('Navigation Sous-service'),
                'render_template'	    => 'template-parts/block/sub-service.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'awards',
                'keywords'			    => array('sous', 'sub', 'service'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                            //'mode' => false,
                                        ),
            )
        );
        
        acf_register_block_type(
            array(
                'name'				    => 'synthesis',
                'title'				    => __('Synthèse témoignage'),
                'description'		    => __('Synthèse à placer en bas d\'une page témoignage - Les avantages de l\'approche Digitemis'),
                'placeholdser'		    => __('Synthèse témoignage '),
                'render_template'	    => 'template-parts/block/synthesis.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-status',
                'keywords'			    => array('témoignage', 'testimonial', 'Synthèse', 'synthesis', 'avantages'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'next-testimonial',
                'title'				    => __('Témoignage suivant'),
                'description'		    => __('Lien vers un témoignage'),
                'placeholdser'		    => __('Témoignage suivant'),
                'render_template'	    => 'template-parts/block/next-testimonial.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'format-status',
                'keywords'			    => array('témoignage', 'testimonial', 'next', 'suivant', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'demarche-legend',
                'title'				    => __('Légende (notre démarche)'),
                'description'		    => __('Légende à placer en bas du schéma "Notre démarche"'),
                'placeholdser'		    => __('Légende (notre démarche)'),
                'render_template'	    => 'template-parts/block/demarche-legend.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'editor-paragraph',
                'keywords'			    => array('Légende', 'texte', 'Démarche'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'formulaire',
                'title'				    => __('Formulaire'),
                'description'		    => __('Formulaire pour planifier une formation'),
                'placeholdser'		    => __('Formulaire'),
                'render_template'	    => 'template-parts/block/formulaire.php',
                'category'			    => 'digitemis',
                'mode'                  => 'preview',
                'icon'				    => 'email',
                'keywords'			    => array('Form', 'Formulaire', 'Formation'),
                'supports'	            => array(
                                            'align'		=> false,
                                            //'multiple' => false,
                                        ),
            )
        );





        
    }
}

<?php



include_once("inc/contact.php");
include_once("inc/acf.php");
include_once("inc/acf-block.php");
//include_once("inc/settings-gutenberg.php");
include_once("inc/widget.php");
include_once("inc/function-jobs.php");
include_once("inc/clean.php");
include_once("inc/no-comment.php");
include_once("inc/images.php");
include_once("inc/custom-post-type.php");
include_once("inc/breadcrumb.php");
include_once("inc/menu.php");
include_once("inc/enqueue_scripts.php");

//MNG

register_block_style(            
  'core/button',            
  array(                
    'name'  => 'blue-border',                
    'label' => __( 'Bordure bleu' ),            
  )        
);
register_block_style(            
  'core/button',            
  array(                
    'name'  => 'white-border',                
    'label' => __( 'Bordure blanche' ),            
  )        
);

function setupcolor_digitemis(){

  $black = '#000000';
  $grey = '#a4a4a4';
  $white = '#ffffff';
  $blue_regular = '#144660';
  $blue_light = '#1ab8ec';
  $blue_medium = '#3D8695';
  $blue_green = '#54b3ba';
  $purple_regular = '#a2004c';

  add_theme_support(
    'editor-color-palette',
    array(
        array(
            'name'  => esc_html__( 'Black', 'twentytwentyone' ),
            'slug'  => 'black',
            'color' => $black,
        ),
        array(
            'name'  => esc_html__( 'Grey', 'twentytwentyone' ),
            'slug'  => 'grey',
            'color' => $grey,
          ),
        array(
            'name'  => esc_html__( 'White', 'twentytwentyone' ),
            'slug'  => 'white',
            'color' => $white,
        ),
        array(
            'name'  => esc_html__( 'Bleue', 'twentytwentyone' ),
            'slug'  => 'blue',
            'color' => $blue_regular,
        ),
        array(
            'name'  => esc_html__( 'Bleue Moyen', 'twentytwentyone' ),
            'slug'  => 'blue_medium',
            'color' => $blue_medium,
        ),
        array(
            'name'  => esc_html__( 'Bleue Verdâtre', 'twentytwentyone' ),
            'slug'  => 'blue_green',
            'color' => $blue_green,
        ),
        array(
            'name'  => esc_html__( 'Bleue Clair', 'twentytwentyone' ),
            'slug'  => 'blue_light',
            'color' => $blue_light,
        ),
        array(
            'name'  => esc_html__( 'Rouge Violet', 'twentytwentyone' ),
            'slug'  => 'purple_regular',
            'color' => $purple_regular,
        ),
    )
  );
}
add_action('after_setup_theme', 'setupcolor_digitemis');

// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );

function ihag_unregister_taxonomy(){
    register_taxonomy('post_tag', array());
    register_taxonomy('category', array());
	add_post_type_support( 'page', 'excerpt' );
	remove_post_type_support( 'page', 'thumbnail' );
}
//add_action('init', 'ihag_unregister_taxonomy');

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
    if ( isset( $_GET['from_post'] ) ) {
        $my_post = get_post( $_GET['from_post'] );
        if ( $my_post )
            return $my_post->post_content;
    }
    return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts($query) {

  if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
      $query->set('post_type', array("post", "rendez-vous", "solution"));
      $query->set('posts_per_page', -1);
  } 

}
//add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL($url)  {
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}



/*
add_action( 'wp_ajax_readMorePost', 'ihag_readMorePost' );
add_action( 'wp_ajax_nopriv_readMorePost', 'ihag_readMorePost' );
function ihag_readMorePost(){
  $offset = ((int)sanitize_text_field( $_POST["page"] ) - 1)  * get_option( 'posts_per_page' );
  $args = array(
    'posts_per_page' => get_option( 'posts_per_page' ),
    'post_type'   => "post",
    'post_status' => 'publish',
    'offset'  => $offset,
    
  );

  if(!empty($_POST['category'])){
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'category',
        'field'    => 'term_id',
        'terms'    => $_POST["category"]
      )
      );
  }  
  $custom_query = new WP_Query($args);
  if ( $custom_query->have_posts() ) : 
    while ( $custom_query->have_posts() ) : 
        $custom_query->the_post();
		    get_template_part( 'template-parts/content', get_post_type() );
    endwhile;
  endif;
  
  wp_die();
}*/

function my_theme_setup() {
  	
  // Nouveauté à ajouter
  add_theme_support('editor-styles');
  add_editor_style( 'style-editor.css' );
  add_editor_style(get_stylesheet_directory_uri().'/style-editor.css');
  add_theme_support( 'align-wide' );
  add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );


 // Add custom validation for CF7 form fields
 function is_company_email($email){ // Check against list of common public email providers & return true if the email provided *doesn't* match one of them

  if(
          preg_match('/@gmail.com/i',   $email) ||
          preg_match('/@gmail.fr/i',    $email) ||
          preg_match('/@hotmail.com/i', $email) ||
          preg_match('/@hotmail.fr/i',  $email) ||
          preg_match('/@live.com/i',    $email) ||
          preg_match('/@msn.com/i',     $email) ||
          preg_match('/@aol.com/i',     $email) ||
          preg_match('/@yahoo.com/i',   $email) ||
          preg_match('/@yahoo.fr/i',    $email) ||
          preg_match('/@inbox.com/i',   $email) ||
          preg_match('/@gmx.com/i',     $email) ||
          preg_match('/@gmx.fr/i',      $email) ||
          preg_match('/@mail.ru/i',     $email) ||
          preg_match('/@rambler.ru/i',  $email) ||
          preg_match('/@test4xrumer.store/i',$email) ||
          preg_match('/@yandex.ru/i',   $email) ||
          preg_match('/@neuf.fr/i',     $email) ||
          preg_match('/@sfr.fr/i',      $email) ||
          preg_match('/@me.com/i',      $email)

  ){
          return false; // It's a publicly available email address
  }else{
          return true; // It's probably a company email address
  }
}
function your_validation_filter_func($result,$tag){
  $type = $tag['type'];
  $name = $tag['name'];
  if('email' == $type || 'email*' == $type){ // Only apply to fields with the form field name of "company-email"
          $the_value = $_POST[$name];
          if(!is_company_email($the_value)){ // Isn't a company email address (it matched the list of free email providers)
                  //$result['valid'] = false;
                  $result->invalidate( $tag, __("Merci d'indiquer une adresse professionnelle et non une adresse personnelle", "digitemis"));
                  //$result['reason'][$name] = 'Yzou need to provide an email address that isn\'t hosted by a free provider.<br />Please contact us directly if this isn\'t possible.';
          }
  }
  return $result;
}
add_filter( 'wpcf7_validate_email', 'your_validation_filter_func', 10, 2 ); // Email field or contact number field
add_filter( 'wpcf7_validate_email*', 'your_validation_filter_func', 10, 2 ); // Req. Email field or contact number


function wpd_update_taxonomy_args( $taxonomy, $object_type, $args ){
  if( 'category' == $taxonomy ){
      remove_action( current_action(), __FUNCTION__ );
      $args['rewrite'] = array( 'slug' => 'blog' );
      register_taxonomy( $taxonomy, $object_type, $args );
  }
}
add_action( 'registered_taxonomy', 'wpd_update_taxonomy_args', 10, 3 );


function ihag_service_rewrite_url() {
    
  
  add_rewrite_rule(
    'services/([^/]+)/([^/]+)/([^/]+)',
    'index.php?post_type=service&name=$matches[3]',
    'top'
  );

  /*add_rewrite_rule(
    'services/([^/]+)/([^/]+)',
    'index.php?post_type=service&name=$matches[2]',
    'top'
  );*/

  /*add_rewrite_rule(
    'services/([^/]+)',
    'index.php?post_type=service&name=$matches[1]',
    'top'
  );*/

  //flush_rewrite_rules();
}
add_action( 'init', 'ihag_service_rewrite_url' );

function ihag_update_service_link( $post_link, $post ) {
	if ( 'service' != $post->post_type )return $post_link;
	
	$id_parent_page = get_field('parent_page', $post->ID);
	if($id_parent_page){
		return get_site_url().'/services'.str_replace(get_site_url(), '', get_permalink($id_parent_page)).$post->post_name;
	}	

	return $post_link; 
}
add_filter( 'post_type_link', 'ihag_update_service_link', 10, 2 );

function check_nonce(){ 
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}
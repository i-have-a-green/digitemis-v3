	</div><!-- div #content -->

	<footer id="footer">
		<div id="footer-top">
			<div id="footer-top-content" class="wrapper">
				<?php
				$field_footer = get_field('prefooter');
				if ( $field_footer == 'newsletter') {
					get_template_part('template-parts/footer/footer-newsletter');
				}elseif ( $field_footer == 'recrutement') {
					get_template_part('template-parts/footer/footer-recrutement');
				}elseif (empty($field_footer)) {
					get_template_part('template-parts/footer/footer-newsletter');
				}
				?>
			</div>
		</div>
		<div id="footer-bottom">
			<div class="wrapper">
				<a id="footer-logo" href="<?php echo get_home_url(); ?>">
					<?php $image = get_field('logo_footer', 'option');
						$size = '150';
						if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}?>
				</a>
				<div id="footer-links-container">
					<div>
						<div id="footer-social" class="social-icon">
							<?php
								if( get_field('linkedin', 'option') ): ?>
									<a href="<?php the_field('linkedin', 'option'); ?>" target="_blank"  data-trk-social="linkedin">
										<img  src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin-white.svg" alt="Linkedin" height="20" width="21">
									</a>
							<?php endif;
							if( get_field('facebook', 'option') ): ?>
								<a href="<?php the_field('facebook', 'option'); ?>" target="_blank"  data-trk-social="Facebook">
									<img  src="<?php echo get_stylesheet_directory_uri(); ?>/image/facebook-white.svg" alt="Facebook" height="20" width="9">
								</a>
							<?php endif;
							if( get_field('twitter', 'option') ): ?>
								<a href="<?php the_field('twitter', 'option'); ?>" target="_blank"  data-trk-social="Twitter">
									<img  src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter-white.svg" alt="Twitter" height="20" width="24">
								</a>
							<?php endif;
							if( get_field('youtube', 'option') ): ?>
								<a href="<?php the_field('youtube', 'option'); ?>" target="_blank"  data-trk-social="youtube">
									<img  src="<?php echo get_stylesheet_directory_uri(); ?>/image/youtube-white.svg" alt="Youtube" height="20" width="29">
								</a>
							<?php endif; ?>
						</div>
						<div id="copyright">&copy;<?php  echo date("Y"); ?> <?php echo get_bloginfo( 'name' );?></div>
						<div id="footer-legal" class="white">
							<?php echo ihag_menu('legal'); ?>
						</div>
					</div>

					<div id="footer-menu" class="mobile-hidden" class="white">
						<?php echo ihag_menu('footer'); ?>
					</div>

					<div id="footer-contact">
						<?php 
						the_field('text_footer', 'option');
						$link = get_field('link_footer', 'option');
						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="button uppercase" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>

				</div><!-- #footer-links-container -->
			</div>
		</div><!-- .wrapper -->
	</footer>

<?php wp_footer(); ?>

</body>
</html>

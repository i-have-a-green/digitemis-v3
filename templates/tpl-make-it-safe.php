<?php
/**
 * Template Name: Make it Safe
 *
 */
get_header();
?>

<main id="main">
	
	<div id="page-<?php the_ID(); ?>">

		<?php if (have_posts()) : while (have_posts()) : the_post();?>
		
		<!-- Content -->
		<div id="gutenberg-content">
			<?php the_content();?>  
		</div>

		<?php endwhile; endif; ?>

	</div><!-- #page-<?php the_ID(); ?> -->
	
</main><!-- #main -->

<?php get_footer(); ?>
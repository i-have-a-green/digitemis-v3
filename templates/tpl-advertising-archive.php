<?php
/**
 * Template Name: Advertising archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
    <div id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Hero thumbnail -->
		<?php get_template_part( 'template-parts/content', 'hero-only' ); ?>

        <section class="wrapper above-hero">      
            <div class="post-container"> 
                <?php
                global $post;
                $args = array( 
                        'posts_per_page'   => -1,
                        'post_type'        => 'advertising',
                        'post_status'      => 'publish'
                    );
                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : 
                setup_postdata( $post ); 
                ?>
                    <div class="single-post single-advertising center">
                        <?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail("thumb-post");
                        } else {
                            echo '<div class="post-no-thumbnail"></div>';
                        } ?>
                        <h2 class="small-content-title single-advertising-title"><?php the_title();?></h2>
                        <?php the_field("content");?>
                        <?php if(get_field("file")):?>
                        <a class="button button-brd-blue" href="<?php the_field("file"); ?>" download>
                            <?php _e("Télécharger", "digitemis");?>
                        </a>
                        <?php endif; ?>
                        <?php if(get_field("link")):
                            $link = get_field("link");
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a  class="button button-brd-blue" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <?php echo esc_html( $link_title ); ?>
                            </a>
                        <?php endif;?>
                    </div>
                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </section>

        <!-- Webinar content -->
        <section id="gutenberg-content" class="above-hero">
            <?php the_content();?>
        </section>
        <!-- Webinar content -->

    </div><!-- #page-<?php the_ID(); ?> -->
<main id="main">

<?php endwhile; endif; ?>

<?php
get_footer();
?>

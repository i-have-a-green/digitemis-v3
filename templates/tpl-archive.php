<?php
/**
 * Template Name: Archive blog
 *
 */
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

get_header();
?>

<main id="main">

    <div id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Breadcrumb -->
        <?php 
        $blog_thumbnail = get_field('blog_thumbnail', 'option');
        if ( $blog_thumbnail ) : ?>
            <style> 
                #page-<?php the_ID();?>::after {
                    background-image: url(<?php echo $blog_thumbnail ?>);
                }
            </style>
        <?php endif; ?>

        <?php
        $allow_breadcrumb = get_field('allow_breadcrumb', 'option');
        if ($allow_breadcrumb) {
            wpBreadcrumb(); 
        }
        ?>

        <!-- Archive title -->
        <section class="wrapper white-bg above-hero">
            <div id="post-header" class="chapo sub-wrapper">
                <?php if( get_field('blog_title', 'option') ) { ?>
                    <h1 class="center"><?php the_field('blog_title', 'option');?><br>
                        <span><?php the_field('blog_subtitle', 'option');?></span>
                    </h1>
                    <div class="center">
                        <?php the_field('blog_intro', 'option');?>
                        <?php //the_archive_description('<div class="center">', '</div>') ?>
                    </div>
                <?php } ?>
            </div>
        </section>

        <!-- Posts Content -->
        <section class="wrapper white-bg above-hero">
            <?php if ( have_posts() ) : ?>

                <nav id="category-blog">
                    <?php echo ihag_menu('category'); ?>
                </nav>
                
                <!-- Nav-category - first version -->
                
                <div>
                    <div class="post-container articles block-spacing" id="infinite-list">
                    <?php
                        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                        $articles = get_posts( array(
                            'paged'             => $paged,
                            'posts_per_page'    => get_option( 'posts_per_page' ),
                            'post_type'         => 'post',
                            'post_status'       => 'publish',
                        ) );
                        global $post;
                        foreach($articles as $article){
                            $post = get_post($article->ID);
                            get_template_part( 'template-parts/content', get_post_type() );
                        }
                        wp_reset_postdata();
                    ?>
                    </div>

                    <div>
                        <div id="contLoaderPost">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/image/loader.png" id="loaderPost" alt="loader">
                        </div>
                    </div>
                </div>

            <?php endif;?>
            <?php // the_posts_pagination(); 
            $articles = get_posts( array(
                'posts_per_page'    => -1,
                'post_type'         => 'post',
                'post_status'       => 'publish'
            ) );
            $max_pages = ceil(sizeof($articles) / get_option( 'posts_per_page' ));
            the_posts_pagination(array("total" => $max_pages)); ?>
        </section><!-- Posts Content -->

    </div><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php
get_footer();

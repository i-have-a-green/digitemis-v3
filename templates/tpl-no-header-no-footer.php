<?php
/**
 * Template Name: no-header no-footer
 *
 */
get_header();
?>

<main id="main">	
	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<?php if (have_posts()) : while (have_posts()) : the_post();?>

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		
		<!-- Page-content -->
		<div id="post-content" class="above-hero">
			<?php the_content();?>
		</div>

		<?php endwhile; endif; ?>

	</div><!-- #page-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php
get_footer();
?>
<?php
/**
 * Template Name: Contact
 *
 */
get_header();
?>

<main id="main">	
	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<?php if (have_posts()) : while (have_posts()) : the_post();?>

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		
		<!-- Page-content -->
		<div id="post-content" class="above-hero">
			<?php the_content();?>
		</div>

		<?php endwhile; endif; ?>

		
		<section  class="wrapper-narrow-container narrow-form above-hero">
			<form class="form-white-background has-section" name="contactForm" id="contactForm" action="#" method="POST">
				<h1 class="center h2-like full-width"><?php  _e("Besoin de plus d'informations ?", "digitemis");?></h1>
				<input type="hidden" name="honeyPotWhitePaper" value="">
				<div class=" half-width">
					<label for="name"><?php  _e("Nom", "digitemis");?> *</label>
					<input type="text" name="name" id="name" placeholder="Garnier" required value="">
				</div>
				<div class=" half-width">
					<label for="firstname"><?php  _e("Prénom", "digitemis");?> *</label>
					<input type="text" name="firstname" id="firstname" placeholder="Alain" required value="">
				</div>
				
				<div class=" half-width">
					<label for="email"><?php  _e("Mail", "digitemis");?> *</label>
					<input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="">
				</div>
				
				<div class=" half-width">
					<label for="phone"><?php  _e("Téléphone", "digitemis");?> *</label>
					<input type="tel" name="phone" id="phone" placeholder="06 XX XX XX XX" required >
				</div>

				<div class=" full-width">
					<label for="company"><?php  _e("Société", "digitemis");?> *</label>
					<input type="text" name="company" id="company" placeholder="SARL Acme" required value="">
				</div>

				<div class=" full-width">
					<label for="postal"><?php  _e("Code postal", "digitemis");?></label>
					<input type="text" name="postal" id="postal" placeholder="44000">
				</div>

				<div class=" full-width">
					<label for="activity"><?php  _e("Secteur d'activité", "digitemis");?> *</label>
					<input type="text" name="activity" id="activity" placeholder="<?php  _e("Communication", "digitemis");?>" value="">
				</div>

				<div class=" full-width">
					<label for="function"><?php  _e("Fonction", "digitemis");?> *</label>
					<input type="text" name="function" id="function" placeholder="<?php _e("Responsable technique", "digitemis");?>" required value="">
				</div>

				
				<div class=" full-width">
					<label for="comment"><?php  _e("Message", "digitemis");?> *</label>
					<textarea rows="5" name="comment" id="comment" placeholder="<?php  _e("Rédigez votre message …", "digitemis");?>" required value=""></textarea>
				</div>

				<div id="ResponseAnchor" class="center full-width contain-button">
					<input class="button button-purple" type="submit" id="sendMessage" value="<?php  _e("Envoyer", "digitemis");?>">
				<div id="ResponseMessage">
					<?php  _e("Votre message a été envoyé !", 'IHAG');?>
				</div>

				<div class="full-width center">
					<i class="formInfo">* <?php  _e("Informations obligatoires", "digitemis");?></i>
					<i class="formInfo"><?php echo sprintf(__("Les informations recueillies à partir de ce formulaire sont traitées par %s pour donner suite à votre demande de contact. Pour connaître vos droits et la politique de %s sur la protection des données,", "digitemis"), get_bloginfo( 'name' ), get_bloginfo( 'name' ));?>
						<a href="<?php  echo get_privacy_policy_url() ?>">
						<?php  _e("Cliquez ici", "digitemis");?>
						</a>
					</i>
				</div>
			</form>
		</section>
		

		<section id="contact-details" class="white blue-bg center bloc-vertical-spacing">
			<div class="wrapper">
				<h2 class="center underline underline-white">
					<?php the_field('phone', 'option'); ?><br>
					<?php the_field('mail', 'option'); ?></h2>
				<div class="contact-loop small-content">
				<?php $items = get_field('place', 'option'); ?>
				<?php if($items): foreach ($items as $key => $item) :?>
					<div class="contact-item">
						<h3 class="small-content-title">
							<?php echo $item['city'];?>
						</h3>
						<p><?php echo $item['adress_1'];?><br><?php echo $item['adress_2'];?></p>
					</div>
				<?php endforeach;endif;?>
			</div>
		</section>

	</div><!-- #page-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php
get_footer();
?>
<?php
/**
 * Template Name: Testimonials archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<main id="main">

    <div id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Breadcrumb -->
        <div class="wrapper">
            <?php get_template_part( 'template-parts/content', 'hero' ); ?>
        </div>
        <!-- Breadcrumb -->
        
        <!-- Page title -->
        <section  class="wrapper above-hero">
            <div class="sub-wrapper white-bg">
                <h1 class="page-title no-padding center"><?php the_title();?></h1>
            </div>
            
            <!-- Page content -->
            <div id="gutenberg-content" >
                <?php the_content() ?>
            </div>
        </section>

        <!-- Testimonials Archive-->
        <section  class="wrapper above-hero">
            <div class="sub-wrapper">
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'categorie',
                ) );
                ?>
                <!-- Testimonial menu -->
                <nav id="nav-category">
                    <ul>
                        <?php foreach ( $terms as $term ) :?>
                            <li class="js-change-categorie " data-category="<?php echo $term->term_id;?>">
                                <?php echo '<h2 class="a-like">'.$term->name . '</h2>';?>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </nav>
                <!-- Testimonial Listing -->
                <div class="post-container-regular">      
                    <?php
                        global $post;
                        $args = array( 
                                'posts_per_page'   => -1,
                                'post_type'        => 'testimonial',
                                'post_status'      => 'publish',
                                'orderby' => 'menu_order' 
                            );
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : 
                        setup_postdata( $post ); 
                        $terms = get_the_terms($post, 'categorie');
                    ?>
                        <div class="recrutement-card testimonial-card" data-termid="<?php echo (isset ($terms[0])) ? $terms[0]->term_id : '';?>">
                            <div class="recrutement-city">
                                <?php
                                $image = get_field("logo");
                                $size = 'costumer';
                                if( $image ) {
                                    echo wp_get_attachment_image( $image, $size );
                                }
                                ?>
                            </div>
                            <div class="recrutement-job" style="background-color:<?php the_field("couleur");?>">
                                <?php the_field("excerpt");?>
                                <a class="button button-brd-white" href="<?php the_permalink(); ?>">
                                    <?php _e("Voir le témoignage", "digitemis");?>
                                </a>
                            </div> 
                        </div>
                    <?php endforeach;
                    wp_reset_postdata(); ?>
                </div><!--.post-container-regular -->
            </div>
        </section><!-- Testimonials Archive -->
        
    </div><!-- #page-<?php the_ID(); ?> -->

</main>
<?php endwhile; endif; ?>

<?php
get_footer();
?>

<?php
/**
 * Template Name: Famille Service archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<!-- Hero thumbnail -->
		<?php get_template_part( 'template-parts/content', 'hero-only' ); ?>

		<!-- Service Title -->
		<div class="wrapper">
			<div id="service-header" style="background-color:<?php the_field("color");?>">
				<h1 class="page-title no-padding center white"><?php the_title();?></h1>
				<h2 class="service-subtitle center white"><?php the_field("sub-title") ?></h2>
			</div><!-- #service-header -->
		</div>

		<!-- #service-content -->
		<div id="service-content" style="color:<?php the_field("color");?>">
			<?php the_content();?>
		</div>

	</div><!-- #page-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();
?>

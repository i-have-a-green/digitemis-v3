<?php
/**
 * Template Name: Webinar archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">

	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<!-- Breadcrumb -->
		<div class="wrapper">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		<!-- Breadcrumb -->

		<!-- Title -->
		<section class="wrapper above-hero">
			<div class="sub-wrapper white-bg">
				<h1 class="page-title no-padding center"><?php the_title();?></h1>
			</div>
		</section>

		<!-- Page-content -->
		<div id="sub-wrapper-content" class="above-hero">
			<?php the_content();?>
		</div>

		<!-- Webinar listing -->
		<section  class="wrapper  bloc-vertical-spacing above-hero">
			<div class="post-container-regular white-bg">
			<?php
			global $post;
				$posts = get_posts( array(
					'post_type'			=> 'webinar',
					'posts_per_page' 	=> -1,
					'post_status'    	=> 'publish',
					'orderby' => 'menu_order',
					/*'meta_query' => array(
						array(
							'key' => 'date',
							'value' => date('Ymd'),
							'type' => 'DATE',
							'compare' => '>='
						)
						),*/

				) );
			?>

			<?php
			if ( $posts ) {
				foreach ( $posts as $post ) :
					setup_postdata( $post ); 
					$terms = get_the_terms($post, 'ville');
					$ville = $terms[0];
					?>
					<?php get_template_part( 'template-parts/content', 'event' );?>
				<?php
				endforeach; 
				wp_reset_postdata();
			}
			?>
			</div>
		</section>
		<!-- Webinar listing -->

		<!-- Last events -->
		<section class="wrapper above-hero">

			<!--<h2 class="page-title underline center margin-top"><?php echo sprintf(__("%s vient à votre rencontre", 'digitemis'), get_bloginfo( 'name' ));?></h2>-->

			<?php
			global $post;
			$events = get_posts( array(
				
				'posts_per_page' => 2,
				'post_status'    => 'publish',
				'include'		=> get_field("evenements_associes")
			) );
			
			?>
			<div class="post-container bloc-vertical-spacing above-hero">
				
				<?php
					if ( $events ) {
						foreach ( $events as $post ) : ?>
							
							<?php get_template_part( 'template-parts/content', 'post' );?>
						
						<?php endforeach; 
					}
				?>
			</div><!-- .post-container -->
		</section>
	</div><!-- #page-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();


if (document.forms.namedItem("contactForm")) {


    var el = document.getElementById('email');

    el.addEventListener("keyup", function (event) {
        var email = el.value;
        if(
          email.match(/@gmail.com/i) ||
          email.match(/@gmail.fr/i) ||
          email.match(/@hotmail.com/i) ||
          email.match(/@hotmail.fr/i) ||
          email.match(/@live.com/i) ||
          email.match(/@msn.com/i) ||
          email.match(/@aol.com/i) ||
          email.match(/@yahoo.com/i) ||
          email.match(/@yahoo.fr/i) || 
          email.match(/@inbox.com/i) ||
          email.match(/@gmx.com/i) ||
          email.match(/@gmx.fr/i) ||
          email.match(/@mail.ru/i) ||
          email.match(/@rambler.ru/i) ||
          email.match(/@test4xrumer.store/i) ||
          email.match(/@yandex.ru/i) ||
          email.match(/@neuf.fr/i) ||
          email.match(/@sfr.fr/i) ||
          email.match(/@me.com/i) 
        ){
            el.setCustomValidity("Veuillez renseigner une adresse email professionnelle");
        } else {
            el.setCustomValidity("");
        }
    });

    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();


       

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                window.dataLayer.push({
                    "event":"contact-form-success"
                });
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("contactRecrutement")) {
    document.forms.namedItem("contactRecrutement").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactRecrutement");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactRecrutement', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("contactLivreBlanc")) {
    document.forms.namedItem("contactLivreBlanc").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactLivreBlanc");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'contactLivreBlanc', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                window.dataLayer.push({
                    "event":"telechargement-livre-blanc"
                });
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}
if (document.forms.namedItem("formRegistration")) {
    document.forms.namedItem("formRegistration").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("formRegistration");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'formRegistration', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}

if (document.forms.namedItem("footer-newslettter-form")) {
    document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
        document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
        e.preventDefault();
        var form = document.forms.namedItem("footer-newslettter-form"); 
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl.resturl + 'formNewsletter', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            console.log(xhr.status);
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('sendMessageNewsletter').disabled = false;
                document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageNewsletter').innerHTML = xhr.response.replace('"', '').replace('"', '');
                document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
                window.dataLayer.push({
                    "event":"newsletter-success"
                });
            }
        };
        xhr.send(formData);
    });
}

if (document.getElementById("wpcf7-f49253-p49254-o1")) {
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        window.dataLayer.push({
            "event":"telechargement-fiche-pratique"
        });
    }, false );
}

if (document.getElementById("wpcf7-f49425-p49357-o1")) {
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        window.dataLayer.push({
            "event":"telechargement-presentation"
        });
    }, false );
}


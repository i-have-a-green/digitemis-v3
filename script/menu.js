document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("link-menu").addEventListener("click", function(e) {
        e.preventDefault();
        document.getElementById("menu-mobile-container").classList.toggle("openMenu");
        document.getElementById("link-menu").classList.toggle("close-menu");
    });
    //document.getElementById("menu").addEventListener("click", function(e) {
    //    document.getElementById("menu").classList.remove("openMenu");
    //});
});

// topbar gets sticky on scroll
var scroll = document.getElementById("masthead");
var menu = document.getElementById("masthead");
var sticky = 1;
window.onscroll = function() {
    if (window.pageYOffset >= sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
};
document.addEventListener("DOMContentLoaded", function(event) {
    if (window.pageYOffset >= sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
});

function toggleSearchbar() {
    document.getElementById("form-search").classList.toggle("openSearchbar");
}


var els = document.getElementsByClassName("js-change-categorie");
Array.prototype.forEach.call(els, function(elm) {
    elm.addEventListener("click", function(e) {
        noActive(els);
        elm.classList.add("active");
        termIdTestimonial = elm.dataset.category;
        var testimonials = document.getElementsByClassName("testimonial-card");
        Array.prototype.forEach.call(testimonials, function(testimonial) {
            console.log(testimonial.dataset.termid + " - " + termIdTestimonial);

            if (testimonial.dataset.termid == termIdTestimonial) {
                testimonial.classList.add("active");
                testimonial.classList.remove("hidden");
            } else {
                testimonial.classList.add("hidden");
                testimonial.classList.remove("active");
            }
        });

    });
});


function noActive(elms) {
    Array.prototype.forEach.call(elms, function(elm) {
        elm.classList.remove("active");
    });
}
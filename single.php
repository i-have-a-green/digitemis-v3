<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
	<article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		
		<!-- Testimonial-title -->
		<section class="wrapper-narrow-container above-hero">
			<div class="wrapper-narrow white-bg">
				<!-- Post-title -->
				<h1 class="page-title center underline"><?php the_title();?></h1>
				<div class="entry-meta">

					<p class="auth-date-info">
					Écrit par
					<?php echo get_the_author_posts_link() ?>
					<?php
						$time_string = 'le <time class="entry-date published updated" datetime="%1$s">%2$s</time>
										, Modifié le
										<time class="entry-date published updated" datetime="%3$s">%4$s</time>';
						echo sprintf( $time_string,
							esc_attr( get_the_date( DATE_W3C ) ),
							esc_html( get_the_date() ),
							esc_attr( get_the_modified_date( DATE_W3C ) ),
							esc_html( get_the_modified_date() )
						);
					?>
					</p>
				</div><!-- .entry-meta -->
				<ul class="single-taxo">
					<?php
					$terms = get_the_terms($post, "post_tag");
					if($terms){
						_e("Sujets :",'digitemis');
						foreach ( $terms as $key => $term ) :?>
						<li>
							<a href="<?php echo get_term_link($term);?>" class="tag">
								<?php echo $term->name;
								if ($key !== array_key_last($terms)) {
									echo ', ';
								}							
								?>
							</a> 
						</li>
						<?php endforeach;
					}
					?>
				</ul>		
			</div>
		</section>
		
		<div id="post-content" class="above-hero">
			<?php if(is_single()):?>
				<?php the_content();?>
			<?php else:?>
				<?php the_excerpt();?>
			<?php endif;?>
			<section id="post-share" class="wrapper-narrow">
				<p><?php _e("Je partage", "digitemis")?></p>

				<a class="JSrslink" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/facebook.svg" height="24" width="24">
				</a>
				<a class="JSrslink" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="24" width="24">
				</a>
				<a class="JSrslink" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="24" width="24">
				</a>

			</section>
		</div><!-- #post-content -->

		<?php if (get_field("show_author")): {
			get_template_part('template-parts/author-bio');
			}
		
		endif;?>

		<?php if (get_field('lastposts_toggle') === NULL || get_field('lastposts_toggle')) :?>
		<section  class="wrapper bloc-vertical-spacing above-hero">
			<h2 class="section-title center underline"><?php _e("Derniers articles", 'digitemis');?></h2>
			<?php
			global $post;
				$lastposts = get_posts( array(
					'posts_per_page' => 2,
					'post_status'    => 'publish'
				) );
			?>
			<div class="post-container">
				<?php
					if ( $lastposts ) {
						foreach ( $lastposts as $post ) :?>

						<?php get_template_part( 'template-parts/content', 'post' );?>

						<?php
						endforeach; 
						wp_reset_postdata();
					}
				?>
			</div><!-- .post-container -->
		</section>
		<?php endif;?>
	</article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();

<?php
/**
 * The template for displaying all single Training
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<?php 
	setup_postdata( $post ); 
	$terms = get_the_terms($post, 'ville');
	$ville = $terms[0];
?>

<main id="main">
    <article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Training hero -->
        <div class="wrapper-narrow-container">
            <?php get_template_part( 'template-parts/content', 'hero' ); ?>
        </div>


        <!-- Title -->
        <section class="wrapper-narrow-container above-hero">
            <div class="wrapper-narrow white-bg">
                <!-- Post-title -->
                <a href="<?php the_permalink();?>" class="blue">
                    <h1 class="page-title center underline"><?php the_title();?></h1>
                </a>
                <p class="center" ><?php echo $ville->name;?>&#8195;—&#8195;<?php the_field("date");?></p>
            </div>
        </section>

        <!-- Training content -->
        <section id="post-content" class="above-hero">
            <?php the_content();?>
        </section>

        <!-- Training form
        <section class="narrow-form white-bg above-hero">
            <?php // get_template_part( 'template-parts/content', 'inscription' );?>
        </section>
        Training form -->
        
    </article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();
